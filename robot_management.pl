use strict;
use warnings;

# ************************************************* #
# Chargement des librairies !
# ************************************************* #
use lib "D:/apps/Nimsoft/perllib";
use lib "D:/apps/Nimsoft/Perl64/lib/Win32API";
use Nimbus::API;
use Nimbus::CFG;
use Nimbus::PDS;
use Nimprobe::struct;
use Nimprobe::robotconfig;
use Nimprobe::probe;
use Nimprobe::cmdb;

# ************************************************* #
# Chargement des packages !
# ************************************************* #
use Libs::Tools;
use Cwd;
use Time::HiRes qw( time );
use DBI;
use Term::ANSIColor qw(:constants);
use Win32::Console::ANSI;
use File::Copy;
use File::Path 'rmtree';
use Data::Dumper;
use Net::SMTP;
$Data::Dumper::Indent = 1;

my $ARGS_Active = 0;
my %ARGS_NFO = (
    R => 1, # Remove reliqua
    U => 1, # Update robot (usertag2,domain,secondary,origin).
    M => 1, # Move robot
    E => 1, # EmaiUN
    N => 1, # Net connect
    F => 0, # FAST MODE, 0 default value!
);

#
# On récupare les arguments de script!
#
if(scalar @ARGV > 0) {
    $ARGS_Active = 1;
    my @ARGS = split(//,substr(uc($ARGV[0]),1));
    my %ARGSHash = map { $_ => 1 } @ARGS;

    for my $arg (keys %ARGS_NFO) {
        if(not exists($ARGSHash{$arg})) {
            $ARGS_NFO{$arg} = 0;
        }
        else {
            $ARGS_NFO{$arg} = 1;
        }
    }
}

sub breakApplication { # Ctrl-C key in console-mode for breaking script !
    print "\n\n!!! CTRL-C BREAKING CONSOLE !!!\n\n";
    exit(1);
}
$SIG{INT} = \&breakApplication;

# ************************************************* #
# Global vars
# ************************************************* #
my $GBL_Time_ScriptExecutionTime = time();
my $GBL_STR_ProbeName = "robot_management";
my $GBL_STR_ProveVersion = "2.4";
my $GBL_Time_ExecutionTimeStart = Libs::Tools::LogTime();
my $GBL_STR_Directory = getcwd;
my $GBL_STR_RemoteHUB = "hub";
my $GBL_STR_Time_Format = "%.2f";
my $GBL_STR_Ouputdir = "output";
my $GBL_STR_CompleteOuputDir = "$GBL_STR_Ouputdir/$GBL_Time_ExecutionTimeStart";
my $GBL_STR_MovingCOUNT = 0;
my $GBL_FailedGrappe = 0;

my $GBL_STATS_COUNT = 0;
my $GBL_STATS_MOVING = 0;
my $GBL_STATS_MOVING_SUCCESS = 0;
my $DB;

# ************************************************* #
# Start NMS PROBE
# ************************************************* #
my $NMS_Probe;
my $NFO_CMDB = "";
my $NFO_IGNORE_CMDB = 0;
my $NFO_IGNORE_PARKING = 0;

# ************************************************* #
# Create directory!
# ************************************************* #
Libs::Tools::createDir("$GBL_STR_Ouputdir");
Libs::Tools::createDir("$GBL_STR_Ouputdir/$GBL_Time_ExecutionTimeStart");
Libs::Tools::createDir("temp");

# COPY THE LOG FILE!
if(-e "$GBL_STR_ProbeName.log") {
    copy("$GBL_STR_ProbeName.log","$GBL_STR_CompleteOuputDir/$GBL_STR_ProbeName-old.log") or warn "Failed to copy the log into the final outdir";
}

$NMS_Probe = new Nimprobe::struct($GBL_STR_ProbeName,$GBL_STR_ProveVersion);
$NMS_Probe->start();

# ************************************************* #
# Read CFG
# ************************************************* #
my $CFG = Nimbus::CFG->new("$GBL_STR_ProbeName.cfg");
my $CFG_GROUP = $CFG->{"pools"};
my $GBL_Fastretrieving;
if($ARGS_NFO{F}) {
    $GBL_Fastretrieving = 1;
}
else {
    $GBL_Fastretrieving = $CFG->{"poolsconfig"}->{"fast_mode"} || 0;
}
my $GBL_NMS_Domain = $CFG->{"poolsconfig"}->{"domain"};
my $GBL_Visio = $CFG->{"poolsconfig"}->{"audit"} || 0;
my $GBL_Presskey = 0;
my $GBL_Safemode = $CFG->{"poolsconfig"}->{"safemode"} || 1;
my %GBL_ExcludedRobots = ();

# Print script start argument!
for my $arg (keys %ARGS_NFO) {
    my $V = $ARGS_NFO{$arg};
    $NMS_Probe->log("Start argument $arg have for value => $V",4);
    $NMS_Probe->localLog("Start argument $arg have for value => $V",4);
}

# ************************************************* #
# DBI
# ************************************************* #
{
    my $DB_User         = $CFG->{"CMDB"}->{"sql_user"};
    my $DB_Password     = $CFG->{"CMDB"}->{"sql_password"};
    my $DB_SQLServer    = $CFG->{"CMDB"}->{"sql_host"};
    my $DB_Database     = $CFG->{"CMDB"}->{"sql_database"};

    $DB = DBI->connect("$DB_SQLServer;UID=$DB_User;PWD=$DB_Password",{
        RaiseError => 1,
        AutoCommit => 0
    }) or die DBI::errstr;

    if(not defined($DB)) {
        $NMS_Probe->log(RED."Unable to connect to the CMDB database!",0);
        $NMS_Probe->localLog("Unable to connect to the CMDB database!",0);
        $NMS_Probe->close();
        exit(1);
    }
    else {
        $DB->do("USE $DB_Database") or die DBI::errstr;
        $NMS_Probe->log(YELLOW."INFO - ".GREEN."The script was successfully authenticate to the CMDB database!",3);
        $NMS_Probe->localLog("The script was successfully authenticate to the CMDB database!",3);
    }
}

# ************************************************* #
# Delete old directory
# ************************************************* #
sub getDate {
    my ($self) = @_;
    my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
    my $timestamp   = sprintf ( "%04d%02d%02d %02d:%02d:%02d",$year+1900,$mon+1,$mday,$hour,$min,$sec);
	$timestamp     =~ s/\s+/_/g;
	$timestamp     =~ s/://g;
    return $timestamp;
}

$NMS_Probe->log("Delete old ouput directory!",3);
$NMS_Probe->localLog("Delete old ouput directory!",3);

{
    my $DATE_ExecutionStartTime = getDate();
    my $CACHE_DAY = 30;
    my $exec_annee  = substr($DATE_ExecutionStartTime,0,4);
    my $exec_month  = substr($DATE_ExecutionStartTime,4,2);
    my $exec_day    = substr($DATE_ExecutionStartTime,6,2);
    my $regular_day = 31;

    my @removeDirectory = ();
    opendir(DIR,'output/');
    my @directory = readdir(DIR);
    foreach(@directory) {
        next if $_ eq '.' || $_ eq '..';
        my $annee   = substr($_,0,4);
        my $month   = substr($_,4,2);
        my $day     = substr($_,6,2);

        if($exec_annee > $annee) {
            push(@removeDirectory,$_);
            next;
        }
        else {
            my $calc = $exec_day - $CACHE_DAY;
            if( $calc <= 0 ) {
                my $average_day = $regular_day - abs($calc);
                if( $average_day > $day && $month != $exec_month ) {
                    push(@removeDirectory,$_);
                    next;
                }
            }
            elsif( $calc > $day ) {
                push(@removeDirectory,$_);
                next;
            }
        }
    }

    foreach(@removeDirectory) {
        $NMS_Probe->log("Remove old output directory => $_",2);
        $NMS_Probe->localLog("Remove old output directory => $_",2);
        rmtree("output/$_");
    }
}

# ************************************************* #
# On daclare la variable CMDB_Hash!
# ************************************************* #
my %CMDB_Hash;

# ************************************************* #
# Parse cfg file!
# ************************************************* #
sub parseGrappe {
    my ($poolProfiles,$poolSection) = @_;
    my %parsedGrappe = ();

    # On foreach les poolsID de chaque grappe (reprasente les pays plus raguliarement!)
    foreach my $poolID (keys $poolProfiles) {
        my $primary         = $poolProfiles->{$poolID}->{"primary"};
        my $primaryRobot    = $poolProfiles->{$poolID}->{"primaryRobot"};
        my $secondary       = $poolProfiles->{$poolID}->{"secondary"} || "";
        my $secondaryRobot  = $poolProfiles->{$poolID}->{"secondaryRobot"} || "";
        my $etiquette       = $poolProfiles->{$poolID}->{"etiquette"};

		# Si l'atiquette est vide on l'asigne a default. Si la personne a oublier le 'D' majuscule ont hotfix aussi...
        if($etiquette eq "" || $etiquette eq "default") {
            $etiquette = "Default";
        }

        # Craation ou Stack d'une nouvelle entrae...
        if(exists $parsedGrappe{$primary}) {
            if( not exists($parsedGrappe{$primary}{etiquette}{$etiquette}) ) {
                $parsedGrappe{$primary}{etiquette}{$etiquette} = 0;
            }
        }
        else {
            $parsedGrappe{$primary} = {
                primary         => $primary,
                primaryRobot    => $primaryRobot,
                secondary       => $secondary,
                reliqualist     => {},
                secondaryRobot  => $secondaryRobot,
                robotcount      => 0
            };
            $parsedGrappe{$primary}{etiquette}{$etiquette} = 0;
        }
    }
    return %parsedGrappe;
}

# ************************************************* #
# Get robots informations on nimsoft!
# ************************************************* #
sub getOnNimsoft {
    my (%GrappeRef) = @_;

    my ($RC,$RES) = Libs::Tools::Request("hub","gethubs");
    if($RC == NIME_OK) {

        my $HUBS_PDS = Nimbus::PDS->new($RES);
        my $ALL_COUNT = 0;
        for( my $count = 0; my $HUB = $HUBS_PDS->getTable("hublist",PDS_PDS,$count); $count++) {
            my $HUBName     = $HUB->get('name');
            my $HUBDomain   = $HUB->get('domain');
            my $HUBRobot    = $HUB->get('robotname');

            # Si le hub n'est pas dans notre Grappe !
            if( not exists($GrappeRef{$HUBName}) ) {
                next;
            }
            $NMS_Probe->log(YELLOW."INFO - ".CYAN."Starting UIM Robots search for hub ".RESET.YELLOW."$HUBName",2);
            $NMS_Probe->localLog("Starting search for hub $HUBName.",2);

            my ($RC_ROBOT,$RQ_Robot) = Libs::Tools::Request("/$HUBDomain/$HUBName/$HUBRobot/hub","getrobots");
            if($RC_ROBOT == NIME_OK) {

                $NMS_Probe->log(YELLOW."INFO - ".GREEN."Recovery of robots from $HUBName is OK.",3);
                $NMS_Probe->localLog("Recovery of robots from $HUBName is OK.",3);
                my $ROBOT_PDS       = Nimbus::PDS->new($RQ_Robot);

				# On initialise les stats a 0.
                my $count_robot     = 0;
                my $static_robot    = 0;
                my $inactif_robot   = 0;

				# On vérifie si le hub est un parking ou non!
				my $isParking = exists($GrappeRef{$HUBName}{etiquette}{"Parking"});

				# On foreach tout les robots du hub!
                for(; my $ROBOTNFO = $ROBOT_PDS->getTable("robotlist",PDS_PDS,$count_robot); $count_robot++) {
                    $ALL_COUNT++;
                    my $ok              = 1;
                    my $ROBOT_STATUS    = $ROBOTNFO->get('status');
                    my $ROBOT_IP        = $ROBOTNFO->get('ip');
                    my $ROBOT_Name      = $ROBOTNFO->get('name');
                    my $ROBOT_Addr      = $ROBOTNFO->get('addr');
                    my $ROBOT_Origin    = $ROBOTNFO->get('origin') || "";
                    my $ROBOT_Usertag2  = $ROBOTNFO->get('os_user2') || "";
                    my $ROBOT_Maintenance = $ROBOTNFO->get('maint_until') ? 1 : 0;
                    my $isApipa         = 0;

                    print "\n";

                    $NMS_Probe->log("Recovery local info from ".GREEN."$ROBOT_Name".RESET." [N' ".YELLOW."$ALL_COUNT".RESET."]",3);
                    $NMS_Probe->localLog("Recovery local info from $ROBOT_Name [N' $ALL_COUNT]",3);

                    $NMS_Probe->log("Origin: ".YELLOW."$ROBOT_Origin".RESET." - Usertag2: ".YELLOW."$ROBOT_Usertag2".RESET."",3);
                    $NMS_Probe->localLog("Origin: $ROBOT_Origin - Usertag2: $ROBOT_Usertag2",3);

                    if(exists($GBL_ExcludedRobots{lc $ROBOT_Name})) {
                        $NMS_Probe->log(MAGENTA."excluded from the processing pool.",2);
                        $NMS_Probe->localLog("excluded from the processing pool.",2);
                        next;
                    }
                    elsif($ROBOT_STATUS ne 0) {
                        $ok = 0;
                        $NMS_Probe->log("server offline ".RESET."[N' ".YELLOW."$static_robot".RESET."]",2);
                        $NMS_Probe->localLog("server offline, [N' $static_robot]",2);
                    }
                    elsif($HUBRobot eq $ROBOT_Name) {
                        $NMS_Probe->log(YELLOW."This is the hub! Switching to the next occurence.",2);
                        $NMS_Probe->localLog("This is the hub! Switching to the next occurence.",2);
                        next;
                    }
                    elsif($ROBOT_IP eq "127.0.0.1" || index($ROBOT_IP, "169.254") != -1) {
                        $ok = 0;
                        $NMS_Probe->log(YELLOW."loopback ip ($ROBOT_IP) ".RESET." [N' ".RED."$static_robot".RESET."]",2);
                        $NMS_Probe->localLog("loopback ip ($ROBOT_IP) [N' $static_robot]",2);
                        $isApipa = 1;
                    }

                    # Check if the robot is exclude from the list!
                    my $ROBOT_DOMAIN = "";
                    my $shub_name = "";
                    my $shub_robotname = "";
                    my $phub_name = "";

                    if($ok && not $GBL_Fastretrieving) {
                        # On varifie que la sonde controller nous rapond!
                        my $PDS = pdsCreate();
                        my ($RC_INFO,$RQ_INFO) = nimNamedRequest("/$HUBDomain/$HUBName/$ROBOT_Name/controller","gethub",$PDS,5);
                        pdsDelete($PDS);
                        if($RC_INFO != NIME_OK) {
                            $ok = 0;
                            $NMS_Probe->log(RED."Callback 'gethub' failed with return code = $RC_INFO ".RESET." [N' ".GREEN."$static_robot".RESET."]".RESET,1);
                            $NMS_Probe->localLog("Callback 'gethub' failed with return code = $RC_INFO",1);
                        }
                        else {
                            my $Robot_HASH = Nimbus::PDS->new($RQ_INFO)->asHash();
                            $ROBOT_DOMAIN       = $Robot_HASH->{domain} || "";
                            $shub_name          = $Robot_HASH->{shub_name} || "";
                            $shub_robotname     = $Robot_HASH->{shub_robotname} || "";
                            $phub_name          = $Robot_HASH->{phub_name} || "";
                        }
                    }

					my $in_CMDB = 1;

                    # On vérifie que le robot est dans la CMDB!
                    if(not exists($CMDB_Hash{lc $ROBOT_Name})) {
						$NMS_Probe->log(RED."Unable to find the robot on the CMDB_Import.Asset_DB table.",2);
                        $NMS_Probe->localLog("Unable to find the robot on the CMDB_Import.Asset_DB table.",2);
						$inactif_robot++;
						$in_CMDB = 0;
                        if(not $isParking) {
                            $GrappeRef{$HUBName}{failedlist}{$ROBOT_Name} = {
                                name            => $ROBOT_Name,
                                status          => $ROBOT_STATUS,
                                addr            => $ROBOT_Addr,
                                origin          => $ROBOTNFO->get('origin'),
                                ip              => $ROBOT_IP
                            };
                        }
						else {
							next;
						}
                    }

                    my @CMDB_NFO = $CMDB_Hash{lc $ROBOT_Name};
                    my $SUPERVISION = $CMDB_NFO[0][3] || 'inactif';
                    my $FinalDestination = "Parking";

                    if($NFO_IGNORE_PARKING) {
                        $FinalDestination = "Default";
                    }

                    if(not $isParking) {
						if($in_CMDB) {
							if($SUPERVISION eq 'inactif' && $ok) {
								$CMDB_NFO[0][0] = $FinalDestination;
							}
						}
						else {
							$CMDB_NFO[0][0] = $FinalDestination;
                            if($isApipa) {
                                $ok = 1;
                            }
						}
                    }
                    else {
                        if(not $GBL_Fastretrieving) {
                            if(lc($SUPERVISION) eq "actif" && $ROBOT_Maintenance) {
                                if(Nimprobe::robotconfig::Maintenance($ROBOT_Addr,"actif")) {
                                    $NMS_Probe->log("$ROBOT_Name set maintenance_mode ".YELLOW."'off'".RESET." done!",3);
                                    $NMS_Probe->localLog("$ROBOT_Name set maintenance_mode 'off' done!",3);
                                }
                                else {
                                    $NMS_Probe->log("$ROBOT_Name set maintenance_mode ".YELLOW."'off'".RESET." failed!",2);
                                    $NMS_Probe->localLog("$ROBOT_Name set maintenance_mode 'off' failed!",2);
                                }
                            }
                        }
                    }

                    my $robotGroup = "robotslist";
                    if(not $ok) {
                        $static_robot++;
                        $robotGroup = "reliqualist";
                    }

                    # On ajoute le robot a son groupe!
                    $GrappeRef{$HUBName}{$robotGroup}{$ROBOT_Name} = {
                        name            => $ROBOT_Name,
                        status          => $ROBOT_STATUS,
                        addr            => $ROBOT_Addr,
                        domain          => $ROBOT_DOMAIN,
                        origin          => $ROBOTNFO->get('origin'),
                        ip              => $ROBOT_IP,
                        etiquette       => $CMDB_NFO[0][0],
                        pilotage_old    => $CMDB_NFO[0][1],
                        centre_orga     => $CMDB_NFO[0][2],
                        os_user1        => $ROBOTNFO->get('os_user1'),
                        os_user2        => $ROBOTNFO->get('os_user2'),
                        source          => $HUBName,
                        shub_name       => $shub_name,
                        shub_robotname  => $shub_robotname,
                        phub_name       => $phub_name,
                        ok              => $ok,
                        supervision     => $SUPERVISION
                    };

                }

                $GrappeRef{$HUBName}{"static"}      = $static_robot;
                $GrappeRef{$HUBName}{"robotcount"}  = $count_robot;
                my $realcount = $count_robot - $static_robot;

				$NMS_Probe->log(YELLOW."STATS - ".RESET."For the hub => ".GREEN."$HUBName",5);
                $NMS_Probe->localLog("Stats for the hub => $HUBName",5);

                $NMS_Probe->log(YELLOW."STATS - ".RESET."reliqua robot => ".GREEN."$static_robot",5);
                $NMS_Probe->localLog("Reliqua robot count => $static_robot.",5);

                $NMS_Probe->log(YELLOW."STATS - ".RESET."normal robot => ".GREEN."$realcount",5);
                $NMS_Probe->localLog("Normal robot count => $realcount.",5);

                $NMS_Probe->log(YELLOW."STATS - ".RESET."inactif supervision robot => ".GREEN."$inactif_robot",5);
                $NMS_Probe->localLog("inactif supervision => $inactif_robot.",5);

                sleep(1);

				# Security for failed HUBS!
                my $totalRobot = $static_robot + $realcount;
				if($GBL_Safemode && $totalRobot > 10) {
					if($static_robot > $realcount && not $isParking) {
						$NMS_Probe->log("[$HUBName] Safemode ON : To much inactive robot on this hub.",0);
						$NMS_Probe->localLog("[$HUBName] Safemode ON : To much inactive robot on this hub.",0);
						$GBL_FailedGrappe = 1;
						last;
					}
				}

            }
            else {
                $NMS_Probe->log(RED."Unable to get nimsoft robots on this hub => $HUBName!",0);
                $NMS_Probe->localLog("Unable to get nimsoft robots on this hub => $HUBName!",0);
                $GBL_FailedGrappe = 1;
                last;
            }

        }
    }
    else {
        $NMS_Probe->log(RED."Unable to get nimsoft hubs!",0);
        $NMS_Probe->localLog("Unable to get nimsoft hubs!",0);
        $GBL_FailedGrappe = 1;
    }

    return %GrappeRef;
}

# ************************************************* #
# Loadbalance robot to the right hub!
# ************************************************* #
sub loadBalancer {
    my (%GrappeRef) = @_;
    my %FinalHASH = ();

    foreach my $HUB ( keys %GrappeRef ) {
        $FinalHASH{$HUB} = {
            name            => $HUB,
            primaryRobot    => $GrappeRef{$HUB}{primaryRobot},
            secondary       => $GrappeRef{$HUB}{secondary},
            secondaryRobot  => $GrappeRef{$HUB}{secondaryRobot},
            reliqualist     => $GrappeRef{$HUB}{reliqualist} || {},
            count           => 0,
            origin_count    => $GrappeRef{$HUB}{robotcount},
            moving          => 0,
            etiquette       => $GrappeRef{$HUB}{etiquette}
        }
    }

    # ************************************************* #
    # Permet de racuparer le hub avec le moins de charge en prenant compte des etiquettes!
    # ************************************************* #
    sub getLower {
        my ($RobotEtiquette,$basedCount,$FHash) = @_;
        my %RefHASH = ();
        my @CountValue = ();
        my $i = 0;

        foreach my $HUB ( keys $FHash ) {
            if( exists($FHash->{$HUB}{etiquette}{$RobotEtiquette}) ) {
                my $count = $FHash->{$HUB}{"$basedCount"};
                push(@CountValue,$count);
                $RefHASH{$i} = $FHash->{$HUB};
                $i++;
            }
        }

        # On forward sur un hub default !
        if($i eq 0) {
            foreach my $HUB ( keys $FHash ) {
                if( exists($FHash->{$HUB}{etiquette}{"Default"}) ) {
                    my $count = $FHash->{$HUB}{"$basedCount"};
                    push(@CountValue,$count);
                    $RefHASH{$i} = $FHash->{$HUB};
                    $i++;
                }
            }
        }

        # On map pour savoir si on tombe sur une egalite de charge!
        my $equals = 0;
        if (keys %{{ map {$_, 1} @CountValue }} == 1) {
            $equals = 1;
        }

        my $index = Libs::Tools::minindex(\@CountValue);
        return $RefHASH{"$index"}{name},$equals;
    }

    # First balancing !
    foreach my $HUB ( keys %GrappeRef ) {
        $NMS_Probe->log(YELLOW."INFO - ".CYAN."Starting Loadbalancing for HUB ".RESET.GREEN."$HUB",3);
        $NMS_Probe->localLog("Starting loadbalancing for HUB $HUB.",3);

        foreach my $ROBOT ( keys %{ $GrappeRef{$HUB}{robotslist} } ) {

            my $RobotEtiquette = $GrappeRef{$HUB}{robotslist}{$ROBOT}{etiquette};
            my ($name,$equals) = getLower($RobotEtiquette,"origin_count",\%FinalHASH);
            if($equals && exists $GrappeRef{$HUB}{etiquette}{$RobotEtiquette}) {
                $name = $HUB;
            }

            if($name ne $HUB) {
                $FinalHASH{$name}{robotslist}{$ROBOT} = $GrappeRef{$HUB}{robotslist}{$ROBOT};
                $FinalHASH{$name}{robotslist}{$ROBOT}{source} = $HUB;
                $FinalHASH{$name}{"count"}++;
                $FinalHASH{$name}{"origin_count"}++;

                $FinalHASH{$HUB}{moving}++;
                $NMS_Probe->log(CYAN."Forwarding robot ".RESET.GREEN."$ROBOT".RESET.CYAN." to the hub ".RESET.GREEN."$name",3);
                $NMS_Probe->localLog("=> Forwarding robot $ROBOT to the hub $name!",3);
            }
            else {
                $FinalHASH{$HUB}{robotslist}{$ROBOT} = $GrappeRef{$HUB}{robotslist}{$ROBOT};
                $FinalHASH{$HUB}{robotslist}{$ROBOT}{source} = $HUB;
                $FinalHASH{$HUB}{"count"}++;
            }

        }
    }

    $NMS_Probe->log("------------------------------",5);
    # Load balancing final
    foreach my $HUB ( keys %FinalHASH ) {
        $NMS_Probe->log(YELLOW."Starting Final Loadbalancing for HUB ".RESET.GREEN."$HUB",3);
        $NMS_Probe->localLog("Starting Final loadbalancing for HUB $HUB.",3);

        foreach my $ROBOT ( keys %{ $FinalHASH{$HUB}{robotslist} } ) {

            my $RobotEtiquette = $FinalHASH{$HUB}{robotslist}{$ROBOT}{etiquette};
            my ($name,$equals) = getLower($RobotEtiquette,"count",\%FinalHASH);
            if($equals) {
                $NMS_Probe->log(GREEN."HUB balancing done",3);
                $NMS_Probe->localLog("HUB balancing done",3);
                last;
            }
            else {

                if($name ne $HUB) {
                    $NMS_Probe->log(CYAN."Send ".RESET.GREEN."$ROBOT".RESET.CYAN." to the hub ".RESET.GREEN."$name",2);
                    $NMS_Probe->localLog("Send $ROBOT to the hub $name!",2);
                    my $sourceHUB = $FinalHASH{$HUB}{robotslist}{$ROBOT}{source};

                    if($sourceHUB eq $name) {
                        $FinalHASH{$sourceHUB}{moving}--;
                    }

                    $FinalHASH{$name}{robotslist}{$ROBOT} = $FinalHASH{$HUB}{robotslist}{$ROBOT};
                    delete $FinalHASH{$HUB}{robotslist}{$ROBOT};
                    $FinalHASH{$HUB}{"count"}--;
                    $FinalHASH{$name}{"count"}++;
                }

            }
        }
    }

    # ************************************************* #
    # Information sur le load-balance final!
    # ************************************************* #
    $NMS_Probe->log("------------------------------",5);
    my $total_robot = 0;
    my $total_mouving = 0;
    foreach my $HUB_Name ( keys %FinalHASH ) {
        my $count = $FinalHASH{$HUB_Name}{'count'};
        my $moving_count = $FinalHASH{$HUB_Name}{'moving'};
        $total_mouving = $total_mouving + $moving_count;
        $total_robot = $total_robot + $count;
        $NMS_Probe->log("HUB ".YELLOW."$HUB_Name".RESET." have ".GREEN."$count".RESET." robots and ".GREEN."$moving_count".RESET." moving robots!",5);
        $NMS_Probe->localLog("HUB $HUB_Name have $count robots and $moving_count moving robots!",5);
    }
    $NMS_Probe->log("------------------------------",5);

    $NMS_Probe->log("Total moving robots => ".GREEN."$total_mouving",5);
    $NMS_Probe->localLog("Total moving robots => $total_mouving!",5);

    $NMS_Probe->log("Total count robots => ".GREEN."$total_robot",5);
    $NMS_Probe->localLog("Total count robots => $total_robot!",5);

    $GBL_STATS_COUNT = $total_robot;
    $NMS_Probe->log("------------------------------",5);
    $GBL_STR_MovingCOUNT = $total_mouving;

    return %FinalHASH;
}

# ************************************************* #
# Update usertag2,origin and secondary hub (if neccesary).
# ************************************************* #
sub UpdateRobots {
    my (%FinalGrappe) = @_;
    $NMS_Probe->log("----------------------------------",5);
    $NMS_Probe->log(YELLOW."INFO - ".GREEN."Robot update started !",3);
    $NMS_Probe->log("\n",3);
    $NMS_Probe->localLog("Robot update started !",3);

    my $domain_update_count = 0;
    my $secondary_update_count = 0;
    my $origin_update_count = 0;
    my $usertag2_update_count = 0;
    my $primary_update_count = 0;

    my %RobotsUpdate_fails = ();

    # FinalGrappe
    foreach my $HUB ( keys %FinalGrappe ) {
        foreach my $ROBOT ( keys %{ $FinalGrappe{$HUB}{robotslist} } ) {

            my $Domain          = $FinalGrappe{$HUB}{robotslist}{$ROBOT}{domain} || "";
            my $shub_name       = $FinalGrappe{$HUB}{robotslist}{$ROBOT}{shub_name};
            my $shub_robotname  = $FinalGrappe{$HUB}{robotslist}{$ROBOT}{shub_robotname};
            my $phub_name       = $FinalGrappe{$HUB}{robotslist}{$ROBOT}{phub_name};
            my $secondaryName   = $FinalGrappe{$HUB}{secondary};
            my $secondaryRobot  = $FinalGrappe{$HUB}{secondaryRobot};
            my $sourceRobot     = $FinalGrappe{$HUB}{primaryRobot};
            my $Origin          = $FinalGrappe{$HUB}{robotslist}{$ROBOT}{origin};
            my $Os_user2        = $FinalGrappe{$HUB}{robotslist}{$ROBOT}{os_user2};
            my $Pilotage_old    = $FinalGrappe{$HUB}{robotslist}{$ROBOT}{pilotage_old};
            my $Centre_orga     = $FinalGrappe{$HUB}{robotslist}{$ROBOT}{centre_orga};
            my $ADDR            = $FinalGrappe{$HUB}{robotslist}{$ROBOT}{addr};
            my $update          = 0;
            my $restart         = 0;

            # Mise a jour de l'origin
            if(defined($Centre_orga)) {
                if( $Origin ne $Centre_orga ) {
                    $origin_update_count++;
                    if($GBL_Visio eq 0) {
                        if(not Nimprobe::robotconfig::Origin($ADDR,$Centre_orga)) {
                            $NMS_Probe->log(YELLOW."$ROBOT: Origin update failed!",1);
                            $NMS_Probe->localLog("$ROBOT: Origin update failed!",1);
                            if(not exists($RobotsUpdate_fails{$ROBOT})) {
                                $RobotsUpdate_fails{$ROBOT} = 1;
                            }
                        }
                        else {
                            $NMS_Probe->log(YELLOW."$ROBOT".RESET.": Update Origin ".YELLOW."$Origin".RESET." to ".GREEN."$Centre_orga.".RESET,3);
                            $NMS_Probe->localLog("$ROBOT: Update Origin $Origin to $Centre_orga.",3);
                            $restart = 1;
                            $update = 1;
                        }
                    }
                    else {
                        $NMS_Probe->log(YELLOW."$ROBOT".RESET.": Update Origin ".YELLOW."$Origin".RESET." to ".GREEN."$Centre_orga.".RESET,3);
                        $NMS_Probe->localLog("$ROBOT: Update Origin $Origin to $Centre_orga.",3);
                    }
                }
            }

            # Mise a jour de os_user2
            if(defined($Pilotage_old)) {
                if( $Os_user2 ne $Pilotage_old ) {
                    $usertag2_update_count++;
                    if($GBL_Visio eq 0) {
                        if(not Nimprobe::robotconfig::Usertag2($ADDR,$Pilotage_old)) {
                            $NMS_Probe->log(YELLOW."$ROBOT: Usertag2 update failed!".RESET,1);
                            $NMS_Probe->localLog("$ROBOT: Usertag2 update failed!",1);
                            if(not exists($RobotsUpdate_fails{$ROBOT})) {
                                $RobotsUpdate_fails{$ROBOT} = 1;
                            }
                        }
                        else {
                            $update = 1;
                            $restart = 1;
                            $NMS_Probe->log(YELLOW."$ROBOT".RESET.": Update Usertag2 ".YELLOW."$Os_user2".RESET." to ".GREEN."$Pilotage_old.".RESET,3);
                            $NMS_Probe->localLog("$ROBOT: Update Usertag2 $Os_user2 to $Pilotage_old.",3);
                        }
                    }
                    else {
                        $NMS_Probe->log(YELLOW."$ROBOT".RESET.": Update Usertag2 ".YELLOW."$Os_user2".RESET." to ".GREEN."$Pilotage_old.".RESET,3);
                        $NMS_Probe->localLog("$ROBOT: Update Usertag2 field $Os_user2 to $Pilotage_old.",3);
                    }
                }
            }

            if( not $GBL_Fastretrieving ) {

                if($phub_name ne $HUB) {
                    $NMS_Probe->log("$ROBOT: Fix wrong primary hub ".YELLOW."$phub_name".RESET." to ".GREEN."$HUB.".RESET,2);
                    $NMS_Probe->localLog("$ROBOT: Fix wrong primary hub $phub_name to $HUB.",2);
                    $restart = 1;
                    $primary_update_count++;
                }

                # Mise a jour du domain!
                if( $Domain ne "$GBL_NMS_Domain" ) {
                    if($GBL_Visio eq 0) {
                        if(not Nimprobe::robotconfig::Domain($ADDR,$GBL_NMS_Domain)) {
                            $NMS_Probe->log(YELLOW."$ROBOT: Update domain fail!",1);
                            $NMS_Probe->localLog("$ROBOT: Update domain fail!",1);
                        }
                        else {
                            $domain_update_count++;
                            $NMS_Probe->log("$ROBOT: Update Domain field set as ".YELLOW."$Domain".RESET." to ".GREEN."$GBL_NMS_Domain.".RESET,3);
                            $NMS_Probe->localLog("$ROBOT: Update Domain field set as $Domain to $GBL_NMS_Domain.",3);
                            $update = 1;
                            $restart = 1;
                        }
                    }
                    else {
                        $NMS_Probe->log("$ROBOT: Update Domain field set as ".YELLOW."$Domain".RESET." to ".GREEN."$GBL_NMS_Domain.".RESET,3);
                        $NMS_Probe->localLog("$ROBOT: Update Domain field set as $Domain to $GBL_NMS_Domain.",3);
                    }
                }

                # Check if the secondary hub is good !
                if($shub_name ne $secondaryName || $shub_robotname ne $secondaryRobot) {
                    my $shub_name_log = $shub_name || "undefined";
                    my $shub_robotname_log = $shub_robotname || "undefined";
                    if($GBL_Visio eq 0) {
                        if(Nimprobe::robotconfig::updateSecondary($HUB,$sourceRobot,$GBL_NMS_Domain,$secondaryName,$secondaryRobot,$ADDR)) {
                            $NMS_Probe->log("$ROBOT: Update ".YELLOW."secondary".RESET." $shub_name_log :: $shub_robotname_log [TO] $secondaryName :: $secondaryRobot .",3);
                            $NMS_Probe->localLog("$ROBOT: Update secondary $shub_name_log :: $shub_robotname_log [TO] $secondaryName :: $secondaryRobot .",3);
                            $secondary_update_count++;
                            $restart = 1;
                        }
                        else {
                            $NMS_Probe->log("$ROBOT: Failed to update secondary",1);
                            $NMS_Probe->localLog("$ROBOT: Failed to update secondary",1);
                        }
                    }
                    else {
                        $NMS_Probe->log("$ROBOT: Update ".YELLOW."secondary".RESET." for ".GREEN."$ROBOT".RESET.", $shub_name_log :: $shub_robotname_log [TO] $secondaryName :: $secondaryRobot .",3);
                        $NMS_Probe->localLog("$ROBOT: Update secondary for $ROBOT, $shub_name_log :: $shub_robotname_log [TO] $secondaryName :: $secondaryRobot .",3);
                    }

                }

            }

            if($GBL_Visio eq 0) {


                if($restart) { # Si mise a jour on restart le controller!
                    if(Nimprobe::robotconfig::RestartController($ADDR)) {
                        $NMS_Probe->log(GREEN."Robot ".RESET.YELLOW."$ROBOT".RESET.GREEN." restart!".RESET,2);
                        $NMS_Probe->localLog("Robot $ROBOT restart!",2);
                    }
                    else {
                        $NMS_Probe->log("Failed to restart robot ".YELLOW."$ROBOT".RESET." for robot udpate!",1);
                        $NMS_Probe->localLog("Failed to restart robot $ROBOT for robot udpate!",1);
                    }
                }

                if($update) {
                    my $PDS = pdsCreate();
                    pdsPut_PCH ($PDS,"robot",$ROBOT);
                    my ($RC,$OBJ) = nimNamedRequest( "$ADDR/controller", "_nis_cache_clean",$PDS,2);
                    pdsDelete($PDS);

                    if($RC == NIME_OK) {
                        $NMS_Probe->log(YELLOW."Nis_cache_clean executed sucessfully!".RESET,3);
                        $NMS_Probe->localLog("Nis_cache_clean executed sucessfully!",3);
                    }
                    else {
                        $NMS_Probe->log(RED."Nis_cache_clean execution failed!".RESET,2);
                        $NMS_Probe->localLog("Nis_cache_clean execution failed!",2);
                    }
                }

                if($restart or $update) {
                    $NMS_Probe->log("Robot hub => ".YELLOW."$HUB".RESET."\n",4);
                    $NMS_Probe->localLog("Robot hub => $HUB",4);
                }
            }

        }
    }

    $NMS_Probe->log("-----------------------------------",5);
    $NMS_Probe->log("\n",5);
    $NMS_Probe->log("Updating ".YELLOW."domain".RESET." count => ".GREEN."$domain_update_count.",5);
    $NMS_Probe->localLog("Updating domain count => $domain_update_count.",5);

    $NMS_Probe->log("Updating ".YELLOW."origin".RESET." count => ".GREEN."$origin_update_count.",5);
    $NMS_Probe->localLog("Updating origin count => $origin_update_count.",5);

    $NMS_Probe->log("Updating ".YELLOW."usertag2".RESET." count => ".GREEN."$usertag2_update_count.",5);
    $NMS_Probe->localLog("Updating os_usertag2 count => $usertag2_update_count.",5);

    $NMS_Probe->log("Updating ".YELLOW."secondary".RESET." count => ".GREEN."$secondary_update_count.",5);
    $NMS_Probe->localLog("Updating secondary count => $secondary_update_count.",5);

    $NMS_Probe->log("Updating ".YELLOW."primary".RESET." count => ".GREEN."$primary_update_count.",5);
    $NMS_Probe->localLog("Updating primary count => $primary_update_count.",5);

    $NMS_Probe->log("\n",5);
    $NMS_Probe->log(GREEN."Robot update ended successfully !",3);
    $NMS_Probe->localLog("Robot update ended successfully",3);
    $NMS_Probe->log("-----------------------------------",5);

    my $file_handler;
    unless(open($file_handler,">", "$GBL_STR_CompleteOuputDir/update_fail.txt")) {
        warn "Unabled to open rejected_files \n";
        return;
    }
    foreach my $RobotName ( keys %RobotsUpdate_fails ) {
        print $file_handler "$RobotName\n";
    }
    close $file_handler;
}

# ************************************************* #
# Move robot to the focus hub!
# ************************************************* #
sub MoveRobots {
    $NMS_Probe->log("------------------------------",5);
    $NMS_Probe->log(YELLOW."INFO - ".GREEN."Starting moving robot process!",3);
    $NMS_Probe->localLog("Starting moving robot process!",3);
    my (%FinalHASH) = @_;
    my %FailedMove = ();
    my $total_movingCount = 0;
    my $total_movingTryCount = 0;
    foreach my $HUB ( keys %FinalHASH ) {
        foreach my $ROBOT ( keys %{ $FinalHASH{$HUB}{robotslist} } ) {
            my $HubSource       = $FinalHASH{$HUB}{robotslist}{$ROBOT}{source};
            my $addr            = $FinalHASH{$HUB}{robotslist}{$ROBOT}{addr};
            my $robotEtiquette  = $FinalHASH{$HUB}{robotslist}{$ROBOT}{etiquette};
            my $HubSecondary    = $FinalHASH{$HUB}{secondary};
            my $PrimaryRobot    = $FinalHASH{$HUB}{primaryRobot};
            my $SecondaryRobot  = $FinalHASH{$HUB}{secondaryRobot};

            if($HubSource ne $HUB) {
                $total_movingTryCount++;
                $NMS_Probe->log("Move ".YELLOW."$ROBOT".RESET." on HUB ".RESET.YELLOW."$HubSource".RESET." TO ".RESET.YELLOW."$HUB".RESET." [".GREEN."$total_movingTryCount".RESET." / $GBL_STR_MovingCOUNT]",3);
                $NMS_Probe->localLog("Move $HubSource to >> $HUB ON ROBOT == $ROBOT !",3);
                if($GBL_Visio eq 0) {

                    my $RC = Nimprobe::robotconfig::MoveRobot("$GBL_NMS_Domain", $HubSource, $HUB, $PrimaryRobot, $HubSecondary, $SecondaryRobot,$ROBOT,$NMS_Probe);
                    if($RC) {
                        $total_movingCount++;
                        # On essaye de restart le controller!
                        if(Nimprobe::robotconfig::RestartController($addr)) {
                            $NMS_Probe->log("Robot $ROBOT restart done!",3);
                            $NMS_Probe->localLog("Robot $ROBOT restart done!",3);

                            if($robotEtiquette eq "Parking") {
                                if(Nimprobe::robotconfig::Maintenance($addr,"inactif")) {
                                    $NMS_Probe->log("Robot $ROBOT set maintenance_mode done!",2);
                                    $NMS_Probe->localLog("Robot $ROBOT set maintenance_mode done!",2);
                                }
                                else {
                                    $NMS_Probe->log("Robot $ROBOT set maintenance_mode failed!",1);
                                    $NMS_Probe->localLog("Robot $ROBOT set maintenance_mode failed!",1);
                                }

                            }
                        }
                        else {
                            $NMS_Probe->log("Failed to restart robot $ROBOT!",2);
                            $NMS_Probe->localLog("Failed to restart robot $ROBOT!",2);
                        }
                        $NMS_Probe->log(YELLOW."INFO - ".GREEN."Robot $ROBOT was moved successfully!",3);
                        $NMS_Probe->localLog("Robot $ROBOT was moved successfully!",3);
                    }
                    else {
                        $FailedMove{$HUB}{$ROBOT} = $FinalHASH{$HUB}{robotslist}{$ROBOT};
                        $FinalHASH{$HubSource}{robotslist}{$ROBOT} = $FinalHASH{$HUB}{robotslist}{$ROBOT};
                        delete $FinalHASH{$HUB}{robotslist}{$ROBOT};
                        $NMS_Probe->log(RED."Failed to move ROBOT $ROBOT.",0);
                        $NMS_Probe->localLog("Failed to move ROBOT $ROBOT.",0);
                    }
                }
            }
        }
    }

    # ::TODO Add retry mechanism here !

    $NMS_Probe->log(GREEN."Totaux d'essaie de mouvement de robot == $total_movingTryCount",5);
    $NMS_Probe->log(GREEN."Totaux de mouvement reussie == $total_movingCount",5);
    $NMS_Probe->localLog("Totaux d'essaie de mouvement de robot == $total_movingTryCount",5);
    $NMS_Probe->localLog("Totaux de mouvement reussie == $total_movingCount",5);

    $GBL_STATS_MOVING = $total_movingTryCount;
    $GBL_STATS_MOVING_SUCCESS = $total_movingCount;

    $NMS_Probe->log(GREEN."Ending moving robot process!",3);
    $NMS_Probe->localLog("Ending moving robot process!",3);
    $NMS_Probe->log("------------------------------",5);
    return %FailedMove;
}

# ************************************************* #
# Mise a jour des net_connects sur les hubs!
# ************************************************* #
sub net_config {
    my (%FinalHASH) = @_;
    $NMS_Probe->log(GREEN."Starting process of NET_CONNECT !",3);
    $NMS_Probe->localLog("--------------NET CONNECT PROCESSING STARTED !--------------",3);

    if(not $NFO_IGNORE_CMDB) {

        # On recupere tout les equipements pour la net_connect
        my %Equipements_HASH = Nimprobe::cmdb::getEquipements($DB,$NFO_CMDB);

        # On essaye d'avoir uniquement les equipements hors robots!
        foreach my $HUB ( keys %FinalHASH ) {
            if( scalar keys %{ $FinalHASH{$HUB}{robotslist} } <= 0 ) {
                next;
            }
            my %robotslist = %{ $FinalHASH{$HUB}{robotslist} };
            foreach my $robotName ( keys %robotslist ) {
                if(exists($Equipements_HASH{lc $robotName}) ) {
                    delete $Equipements_HASH{lc $robotName};
                }
            }

            my %reliqualist = %{ $FinalHASH{$HUB}{reliqualist} };
            foreach my $robotName ( keys %reliqualist ) {
                if(exists($Equipements_HASH{lc $robotName}) ) {
                    delete $Equipements_HASH{lc $robotName};
                }
            }
        }


        # On déclare nos deux hashs qui vont contenir les équipements finaux et les équipements sans IP!
        my %Final_Equipements = ();
        my %Errors_Equipements = ();
        my @Pingable_EquipementsFiles = ();

        # Create the temp ping file!
        {
            my $file_handler;
            my $file_count = 0;
            my $maxRobots_perFile = 300;
            my $robot_count = 0;
            my %Pingable_Hash = ();

            foreach my $robotName ( keys %Equipements_HASH ) {
                if($robot_count % $maxRobots_perFile == 0) {
                    push(@Pingable_EquipementsFiles,"temp/pingEquipments_$file_count.txt");
                    $file_count++;
                    unless(open($file_handler,">", "temp/pingEquipments_$file_count.txt")) {
                        warn "Unabled to open pingEquipments_$file_count.txt \n";
                        return;
                    }
                }
                if(not exists($Pingable_Hash{$robotName})) {
                    print $file_handler "$robotName\n";
                    $Pingable_Hash{$robotName} = 1;
                }
                $robot_count++;
            }

            # On prévient l'ajout du dernier dans la boucle !
            if($robot_count % $maxRobots_perFile != 0) {
                push(@Pingable_EquipementsFiles,"temp/pingEquipments_$file_count.txt");
            }
            close $file_handler;

            $NMS_Probe->localLog("Create $file_count ping files in temporary directory!",2);
            $NMS_Probe->log(GREEN."Create $file_count ping files in temporary directory!",2);
        }

        # On boucle les équipements foreach!
        my $GBL_FILE_FRESULT = "result_ping.txt";
        foreach(@Pingable_EquipementsFiles) {

            # On ping nos machines
            $NMS_Probe->localLog("Ping machines from $_!",3);
            $NMS_Probe->log(GREEN."Ping machines from $_!",3);
            system("ping.exe /loadfile $_ /scomma $GBL_FILE_FRESULT");

            # Add to the final equipments hash!
            if(open(my $RobotsLIST, '<:encoding(UTF-8)',"$GBL_FILE_FRESULT")) {
                my $row;
                while($row = <$RobotsLIST>) {
                    my @parsed = split(",",$row);
                    my $robot = lc $parsed[0];
                    my $ip = $parsed[1];
                    if(defined($ip) && $ip ne "") {
                        my @NFO = $Equipements_HASH{lc $robot};
                        $Final_Equipements{lc $robot} = {
                            name => lc $robot,
                            etiquette => $NFO[0][0] || "Default",
                            ip => $ip
                        };
                    }
                    else {
                        $Errors_Equipements{lc $robot} = $Final_Equipements{lc $robot};
                        $Errors_Equipements{lc $robot}{ip} = $ip;
                    }
                }
            }

            unlink("$_");

        }

        # Delete temporary!
        unlink("$GBL_FILE_FRESULT");

        # On enregistre tout les hostnames qui n'ont pas d'ip dans un fichier texte.
        {
            my $file_handler;
            unless(open($file_handler,">", "$GBL_STR_CompleteOuputDir/noip_servers.txt")) {
                warn "Unabled to open rejected_files \n";
                return;
            }
            foreach my $robotName ( keys %Errors_Equipements ) {
                print $file_handler "$robotName\n";
            }
            close $file_handler;
        }

        # delete root file!

        sub lowerHUBS {
            my ($RobotEtiquette,$FHash) = @_;
            my %RefHASH = ();
            my @CountValue = ();
            my $i = 0;

            foreach my $HUB ( keys $FHash ) {
                if( exists($FHash->{$HUB}{etiquette}{$RobotEtiquette}) ) {
                    push(@CountValue,$FHash->{$HUB}{"count"});
                    $RefHASH{$i} = $FHash->{$HUB};
                    $i++;
                }
            }

            # On forward sur un hub default !
            if($i eq 0) {
                foreach my $HUB ( keys $FHash ) {
                    if( exists($FHash->{$HUB}{etiquette}{"Default"}) ) {
                        push(@CountValue,$FHash->{$HUB}{"count"});
                        $RefHASH{$i} = $FHash->{$HUB};
                        $i++;
                    }
                }
            }

            # On map pour savoir si on tombe sur une egalite de charge!
            my $equals = 0;
            if (keys %{{ map {$_, 1} @CountValue }} == 1) {
                $equals = 1;
            }

            my $index = Libs::Tools::minindex(\@CountValue);
            return $RefHASH{"$index"}{name},$equals;
        }

        # Balance equipment
        my %Hubs_equipments = ();
        foreach my $HUB ( keys %FinalHASH ) {
            $FinalHASH{$HUB}{"count"} = 0;
            $FinalHASH{$HUB}{"name"} = $HUB;
        }

        foreach my $ROBOT ( keys %Final_Equipements ) {
            my $RobotEtiquette = $Final_Equipements{lc $ROBOT}{etiquette} || "Default";
            my ($name,$equals) = lowerHUBS($RobotEtiquette,\%FinalHASH);
            $FinalHASH{$name}{robotslist}{$ROBOT} = $Final_Equipements{lc $ROBOT};
            $FinalHASH{$name}{"count"}++;
        }

    }

	my %Netconnect_PushFailed = ();
    my %Netconnect_ActivationFailed = ();

    foreach my $HUB ( keys %FinalHASH ) {
        $NMS_Probe->log("----------------------------",3);
		$NMS_Probe->localLog("----------------------------",3);
		$NMS_Probe->log(GREEN."Start processing of net_connect on ".RESET.YELLOW."$HUB",3);
		$NMS_Probe->localLog("Start processing of net_connect on $HUB",3);
		if(not defined($FinalHASH{$HUB}{primaryRobot})) {
			$NMS_Probe->log(RED."Processing of net_connect failed for $HUB",0);
			$NMS_Probe->localLog("Processing of net_connect failed for $HUB",0);
			next;
		}
        my $robotName = $FinalHASH{$HUB}{primaryRobot};
        my $FILEPATH = "$GBL_STR_Ouputdir/$GBL_Time_ExecutionTimeStart/$HUB.net_connect.cfg";

        # On racupare la configuration de la net_connect
        if(Nimprobe::probe::getCFG($robotName,$FILEPATH,"$GBL_STR_Ouputdir/$GBL_Time_ExecutionTimeStart/$HUB.net_connect.backup.cfg")) {
            my %etiquettes = %{ $FinalHASH{$HUB}{etiquette} };
			my %robotslist;

			# On varifie que le hub possade en moins 1 robot valide!
			if(scalar keys %{ $FinalHASH{$HUB}{robotslist} } <= 0) {
				$NMS_Probe->log(YELLOW."$HUB doesn't have valid robots!",2);
				$NMS_Probe->localLog("$HUB doesn't have valid robots!",2);
				%robotslist = ();
			}
			else {
				%robotslist = %{ $FinalHASH{$HUB}{robotslist} };
			}

            my %APIPA_Robots = ();

            # Delete non-opere / non-pilote server from Parking net_connect!
            if( exists($etiquettes{"Parking"}) ) {
                foreach my $key (keys %robotslist) {
                    if( lc($robotslist{$key}{supervision}) eq "inactif") {
                        delete $robotslist{$key};
                    }
                }
            }

            # On ajoute les reliquas a la net_connect par précaution
            if(%{ $FinalHASH{$HUB}{reliqualist} } && scalar keys %{ $FinalHASH{$HUB}{reliqualist} } > 0) {
                my %reliqualist = %{ $FinalHASH{$HUB}{reliqualist} };
                foreach my $ReliquaRobot (keys %reliqualist) {
                    my $IP = $reliqualist{lc $ReliquaRobot}{ip};
                    if($IP eq "127.0.0.1" || index($IP, "169.254") != -1) {
                        $APIPA_Robots{$ReliquaRobot} = {
                            name => $ReliquaRobot,
                            etiquette => $reliqualist{$ReliquaRobot}{etiquette}
                        };
                    }
                    elsif($reliqualist{lc $ReliquaRobot}{status} eq 2 && not exists($robotslist{lc $ReliquaRobot}) ) {
                        $robotslist{$ReliquaRobot} = $reliqualist{$ReliquaRobot};
                    }
                }
            }

            # Ping APIPA Server !
            if(keys %APIPA_Robots) {

                # Set apipa.txt
                {
                    my $file_handler;
                    unless(open($file_handler,">", "apipa.txt")) {
                        warn "Unabled to open rejected_files \n";
                        return;
                    }
                    foreach my $robotName ( keys %APIPA_Robots ) {
                        print $file_handler "$robotName\n";
                    }
                    close $file_handler;
                }

                # Ping
                system("ping.exe /loadfile apipa.txt /scomma apipa_result.txt");

                # Parse the APIPA result!
                if(open(my $ApipaLIST, '<:encoding(UTF-8)',"apipa_result.txt")) {
                    my $row;
                    while($row = <$ApipaLIST>) {
                        my @parsed = split(",",$row);
                        my $robot = $parsed[0];
                        my $ip = $parsed[1];
                        if(defined($ip) && $ip ne "") {
                            $APIPA_Robots{lc $robot}{ip} = $ip;
                        }
                        else {
                            delete $APIPA_Robots{lc $robot};
                        }
                    }
                }

                for my $RobotName (keys %APIPA_Robots) {
                    $robotslist{$RobotName} = $APIPA_Robots{$RobotName};
                }

            }

			if(scalar keys %robotslist == 0) {
                $NMS_Probe->log(YELLOW."$HUB robotslist is empty, go to the next robot!",3);
				$NMS_Probe->localLog("$HUB robotslist is empty, go to the next robot!!",3);
				next;
			}

            my $fullpath = "/".$GBL_NMS_Domain."/$HUB/$robotName";

            if($GBL_Visio eq 0) {
                # On daactive la probe!
                my $max_profilecount = 0;
                my $deactive_count = 3;
                while($deactive_count--) {
                    my $deactiveRC = Nimprobe::robotconfig::Probe_deactivate($fullpath,"net_connect");
                    if($deactiveRC) {

                        # Generate processed list of robots per net_connect!
                        my $file_handler;
                        unless(open($file_handler,">", "$GBL_STR_CompleteOuputDir/processed_${HUB}_netconnect.txt")) {
                            warn "Unabled to open rejected_files \n";
                            return;
                        }
                        foreach my $robotName ( keys %robotslist ) {
                            print $file_handler "$robotName\n";
                        }
                        close $file_handler;

                        # On racupare la configuration de la net_connect!
                        my $profileCount = Nimprobe::probe::updateCFG($FILEPATH,\%etiquettes,\%robotslist);
                        $NMS_Probe->log("Number of profiles in this net_connect => ".GREEN."$profileCount",3);
                        $NMS_Probe->localLog("Number of profiles in this net_connect => $profileCount",3);
                        $max_profilecount += $profileCount;

                        # On push la configuration de la net_connect!
                        if(Nimprobe::probe::PushConfig($fullpath,$FILEPATH)) {
                            $NMS_Probe->log("Push of net_connect configuration successfully!",3);
                            $NMS_Probe->localLog("Push of net_connect configuration successfully!.",3);

                            # On raactive la probe net_connect!
                            my $activeRC = Nimprobe::robotconfig::Probe_activate($fullpath,"net_connect");
                            if($activeRC) {
                                $NMS_Probe->log(MAGENTA."net_connect probe is reactived!",3);
                                $NMS_Probe->localLog("net_connect probe is reactived!",3);
                            }
                            else {
                                $Netconnect_ActivationFailed{$HUB} = {
        							fullpath => $fullpath
        						};
                                $NMS_Probe->log(RED."net_connect probe not activated",1);
                                $NMS_Probe->localLog("net_connect probe not activated!",1);
                            }
                        }
                        else {
    						$Netconnect_PushFailed{$HUB} = {
    							fullpath => $fullpath,
    							filepath => $FILEPATH
    						};
                            $NMS_Probe->log(RED."Failed to push net_connect configuration on the HUB $HUB",0);
                            $NMS_Probe->localLog("Failed to push net_connect configuration on the HUB $HUB.",0);
                        }
                        last;

                    }
                    else {
                        $NMS_Probe->log(RED."Failed to deactivate net_connect probe on HUB $HUB!",1);
                        $NMS_Probe->localLog("Failed to deactivate net_connect probe on HUB $HUB!",1);
                        sleep(5);
                    }
                }
                $NMS_Probe->log("Number of totals profiles in this grappe => ".GREEN."$max_profilecount",3);
                $NMS_Probe->localLog("Number of totals profiles in this grappe => $max_profilecount",3);
            }
        }
        else {
            $NMS_Probe->log(RED."Failed to get configuration of the net_connect of the HUB $HUB!",0);
            $NMS_Probe->localLog("Failed to get configuration of the net_connect of the HUB $HUB.",0);
        }
    }

	# On essaye de restart les pushs fail une derniare fois!
	if(scalar keys %Netconnect_PushFailed > 0 && $GBL_Visio eq 0) {
		$NMS_Probe->log(YELLOW."Trying to restart some push of net_connect after fails, waiting 5seconds before...",2);
		$NMS_Probe->localLog("Trying to restart some push of net_connect after fails, waiting 5seconds before...",2);
		sleep(5);
		foreach my $K ( keys %Netconnect_PushFailed ) {
			my %Retry = %{ $Netconnect_PushFailed{$K} };
			my $fullpath = $Retry{fullpath};
			if( Nimprobe::probe::PushConfig($fullpath,$Retry{filepath}) ) {
				$NMS_Probe->log(GREEN."Push of net_connect configuration successfully!",3);
				$NMS_Probe->localLog("Push of net_connect configuration successfully!.",3);

				# On raactive la probe net_connect!
				my $activeRC = Nimprobe::robotconfig::Probe_activate($fullpath,"net_connect");
				if($activeRC) {
					$NMS_Probe->log(MAGENTA."net_connect probe is reactived!",3);
					$NMS_Probe->localLog("net_connect probe is reactived!",3);
				}

			}
			else {
				$NMS_Probe->log(RED."Failed to push net_connect a second time the configuration => $fullpath",1);
				$NMS_Probe->localLog("Failed to push net_connect a second time the configuration => $fullpath",1);
			}
		}
	}

    if(scalar keys %Netconnect_ActivationFailed > 0 && $GBL_Visio eq 0) {
		$NMS_Probe->log(YELLOW."Trying to restart probe net_connect after fails, waiting 5seconds before...",2);
		$NMS_Probe->localLog("Trying to restart probe net_connect after fails, waiting 5seconds before...",2);
		sleep(5);
		foreach my $K ( keys %Netconnect_ActivationFailed ) {
			my %Retry = %{ $Netconnect_ActivationFailed{$K} };
			my $fullpath = $Retry{fullpath};

			# On raactive la probe net_connect!
			my $activeRC = Nimprobe::robotconfig::Probe_activate($fullpath,"net_connect");
			if($activeRC) {
				$NMS_Probe->log(MAGENTA."net_connect probe is reactived!",3);
				$NMS_Probe->localLog("net_connect probe is reactived!",3);
			}
            else {
                $NMS_Probe->log(RED."net_connect probe not activated",1);
                $NMS_Probe->localLog("net_connect probe not activated!",1);
            }

		}
	}

}

# ************************************************* #
# send a Email with all reliqua robot!
# ************************************************* #
sub sendEmail {
    my (%Stats) = @_;

    my $Mail_Active         = $CFG->{'mail'}->{'active'};
    my $Mail_Host           = $CFG->{'mail'}->{'host'};
    my $Mail_From           = $CFG->{'mail'}->{'from'};
    my @Mail_To             = split(",",$CFG->{'mail'}->{'to'});

    $NMS_Probe->log("Sending a email section started!",3);
    $NMS_Probe->localLog("Sending a email section started!",3);

    if($Mail_Active) {
        foreach(@Mail_To) {

            my $smtp = Net::SMTP->new("$Mail_Host",
                Debug => 0,
                Timeout => 4
            ) or die "Unable to contact mail server";

            $smtp->mail("$Mail_From");
            if($smtp->to("$_")) {
                $smtp->data();
                $smtp->datasend("To: $_");
                $smtp->datasend("From: $Mail_From\n");
                $smtp->datasend("Subject: Robot_management $Stats{pool} exacution report! \n");
                $smtp->datasend("\n"); # done with header
                $smtp->datasend("robots counts : $Stats{robot_count}\n");
                $smtp->datasend("robots total movement : $Stats{robot_movement}\n");
                $smtp->datasend("robots successfull movement : $Stats{robot_movement_success}\n");

                $smtp->datasend("\n");
                $smtp->datasend("CMDB_REJECT :\n");
                $smtp->datasend("hub;robotname;ip;addr\n");
                foreach my $array ( @{ $Stats{cmdb_reject} } ) {
                    my $hub = @$array[0] || "undefined";
                    my $robot = @$array[1];
                    my $ip = @$array[2] || "undefined";
                    my $addr = @$array[3] || "undefined";
                    $smtp->datasend("$hub;$robot;$ip;$addr\n");
                }
                $smtp->datasend("\n");

                $smtp->datasend("Local servers :\n");
                $smtp->datasend("hub;robotname;ip;addr\n");
                foreach my $array ( @{ $Stats{server_local} } ) {
                    my $hub = @$array[0] || "undefined";
                    my $robot = @$array[1];
                    my $ip = @$array[2] || "undefined";
                    my $addr = @$array[3] || "undefined";
                    $smtp->datasend("$hub;$robot;$ip;$addr\n");
                }
                $smtp->datasend("\n");

                $smtp->datasend("Servers down :\n");
                $smtp->datasend("hub;robotname;ip;addr;statuts\n");
                foreach my $array ( @{ $Stats{server_down} } ) {
                    my $hub = @$array[0] || "undefined";
                    my $robot = @$array[1];
                    my $ip = @$array[2] || "undefined";
                    my $addr = @$array[3] || "undefined";
                    my $statuts = @$array[4] || "undefined";
                    $smtp->datasend("$hub;$robot;$ip;$addr;$statuts\n");
                }
                $smtp->datasend("\n");

                $smtp->datasend("Servers movement fails :\n");
                $smtp->datasend("hub;robotname;ip;addr;statuts\n");
                foreach my $array ( @{ $Stats{server_failed_move} } ) {
                    my $hub = @$array[0] || "undefined";
                    my $robot = @$array[1];
                    my $ip = @$array[2] || "undefined";
                    my $addr = @$array[3] || "undefined";
                    my $statuts = @$array[4] || "undefined";
                    $smtp->datasend("$hub;$robot;$ip;$addr;$statuts\n");
                }
                $smtp->datasend("\n");

                $smtp->dataend();
            }
            else {
                my $errMsg = $smtp->message();
                $NMS_Probe->log("$errMsg",1);
                $NMS_Probe->localLog("$errMsg",1);
            }
            $smtp->quit;
            $NMS_Probe->log(YELLOW."email send to ".RESET.GREEN."$_".RESET.YELLOW." successfully!",2);
            $NMS_Probe->localLog("email send to $_ successfully!",2);
        }
    }

}

# ************************************************* #
# Remove reliqua robot from the hub!
# ************************************************* #
sub removeReliqua {
    my (%FinalHASH) = @_;
    $NMS_Probe->log("-----------------------------------",5);
    $NMS_Probe->localLog("-----------------------------------",5);
    foreach my $HUB ( keys %FinalHASH ) {

        # ------------------------------------------- #
        # Aucun serveur de reliqua !
        # ------------------------------------------- #
        if(scalar keys %{ $FinalHASH{$HUB}{reliqualist} } eq 0) {
            $NMS_Probe->log(YELLOW."INFO - ".GREEN."No reliqua Robot on $HUB!",3);
            $NMS_Probe->localLog("No reliqua Robot on $HUB!",3);
            next; # On passe directement au prochain hub!
        }

        foreach my $ROBOT ( keys %{ $FinalHASH{$HUB}{reliqualist} } ) {
            my $status      = $FinalHASH{$HUB}{reliqualist}{$ROBOT}{status};
            my $ip          = $FinalHASH{$HUB}{reliqualist}{$ROBOT}{ip};
            my $HubSource   = $FinalHASH{$HUB}{reliqualist}{$ROBOT}{source};

            # On ne traite que les serveurs avec un status 4 ou 2 !
            if($status ne 0 || $ip eq "127.0.0.1" || index($ip, "169.254") != -1) {

                if($GBL_Visio eq 1) {
                    $NMS_Probe->log("Remove ".GREEN."$ROBOT".RESET." from ".GREEN."$HUB",3);
                    $NMS_Probe->localLog("Remove $ROBOT from $HUB.",3);
                }
                else {
                    my $PDS = pdsCreate();
                    pdsPut_PCH($PDS,"name",$ROBOT);
                    my ($RC,$RQ_INFO) = nimNamedRequest("/$GBL_NMS_Domain/$HubSource/$ROBOT/hub","removerobot",$PDS);
                    pdsDelete($PDS);

                    if($RC == NIME_OK) {
                        $NMS_Probe->log("Remove ".GREEN."$ROBOT".RESET." successfully from ".GREEN."$HUB",3);
                        $NMS_Probe->localLog("Remove $ROBOT successfully from $HUB.",3);
                    }
                    else {
                        $NMS_Probe->log("Failed to remove ".YELLOW."$ROBOT".RESET." from HUB ".YELLOW."$HUB".RESET." (RC = $RC).",1);
                        $NMS_Probe->localLog("Failed to remove $ROBOT from HUB $HUB (RC = $RC).",1);
                    }
                }
            }
        }
    }
    $NMS_Probe->log("-----------------------------------",5);
    $NMS_Probe->localLog("-----------------------------------",5);
}


sub pressKey {
    if($GBL_Presskey) {
        local( $| ) = ( 1 );
        print YELLOW."Press <".RESET.GREEN."Enter".RESET.YELLOW."> or <".RESET.GREEN."Return".RESET.YELLOW."> to continue: ".RESET;
        my $resp = <STDIN>;
    }
}

# ************************************************* #
# Main function
# ************************************************* #
sub main {
    # ------------------------------------------- #
    # On foreach les sections des pools.
    # ------------------------------------------- #
    foreach my $poolSection (keys $CFG->{"pools"}) {
        # On log le dabut de la pool.

        # On regarde si la grappe est ignorer ou non!
        my $ignore = $CFG->{"pools"}->{$poolSection}->{ignore} || 0;
        if($ignore) {
            $NMS_Probe->log(CYAN."Exclude $poolSection from script processing!",2);
            $NMS_Probe->localLog("Exclude $poolSection from script processing!",2);
            next;
        }

        # On récupère le reste!
        %GBL_ExcludedRobots = ();
        $NFO_CMDB = $CFG->{"pools"}->{$poolSection}->{cmdb};
        $NFO_IGNORE_CMDB = $CFG->{"pools"}->{$poolSection}->{ignore_cmdb} || 0;
        $NFO_IGNORE_PARKING = $CFG->{"pools"}->{$poolSection}->{ignore_parking} || 0;
        my @Robots_excluded = split(",",$CFG->{"pools"}->{$poolSection}->{exclude_robots});

        if(scalar @Robots_excluded > 0) {
            foreach(@Robots_excluded) {
                $GBL_ExcludedRobots{lc $_} = 1;
            }
        }


        $NMS_Probe->log(CYAN."Start execution of $poolSection section!",3);
        $NMS_Probe->localLog("Start execution of $poolSection section!",3);
        my %Stats = (
            pool => $poolSection,
            robot_count => 0,
            robot_movement => 0,
            robot_movement_success => 0,
            cmdb_reject => [],
            server_local => [],
            server_down => [],
            server_failed_move => []
        );

        # ------------------------------------------- #
        # Dans un premier temps on parse la section qui reprasente une Grappe !
        # ------------------------------------------- #
        $NMS_Probe->log(YELLOW."GrappeParsing <$poolSection> started !",3);
        $NMS_Probe->localLog("GrappeParsing <$poolSection> started !",3);
        my %parsedGrappe = parseGrappe($CFG->{"pools"}->{$poolSection}->{profiles},$poolSection);

        # On racupare le contenu de la CMDB en fonction de la Grappe!
        %CMDB_Hash = Nimprobe::cmdb::getLIST($DB,$CFG->{"pools"}->{$poolSection}->{cmdb});

        # ------------------------------------------- #
        # On racupare / enrichie un Hash grace aux informations de nimsoft (callback & CMDB).
        # ------------------------------------------- #
        my %FinalGrappe = getOnNimsoft(%parsedGrappe);
        if($GBL_FailedGrappe) {
        	$NMS_Probe->log(RED."Failed to get UIM informations",0);
        	$NMS_Probe->localLog("Failed to get UIM informations",0);
        	$GBL_FailedGrappe = 0;
        	next;
        }
        pressKey();

        # ------------------------------------------- #
        # Craation de fichier texte avec toutes les informations sur les robots down, maintenance etc...
        # ------------------------------------------- #
        # Rejected file!
        if(not $GBL_Fastretrieving){
            $NMS_Probe->log(YELLOW."Generate cmdb_reject file!",5);
    		$NMS_Probe->localLog("Generate cmdb_reject file!",5);
            my $file_handler;
            unless(open($file_handler,">", "$GBL_STR_CompleteOuputDir/cmdb_reject.txt")) {
                warn "Unabled to open rejected_files \n";
                return;
            }
            foreach my $HUB ( keys %FinalGrappe ) {
                foreach my $robotName ( keys %{ $FinalGrappe{$HUB}{failedlist} } ) {
                    my $ip      = $FinalGrappe{$HUB}{failedlist}{$robotName}{ip};
                    my $addr    = $FinalGrappe{$HUB}{failedlist}{$robotName}{addr};
                    my $robot   = $FinalGrappe{$HUB}{primaryRobot};
                    print $file_handler "$HUB;$robotName;$ip;$addr\n";
                    my @Info = ($HUB,$robotName,$ip,$addr);
                    push($Stats{"cmdb_reject"},\@Info);
                    if($GBL_Visio eq 0 && $ARGS_NFO{U}) {
                        $NMS_Probe->log(YELLOW."$GBL_NMS_Domain/$HUB/$robot set to NON PILOTE",3);
                		$NMS_Probe->localLog("$GBL_NMS_Domain/$HUB/$robot set to NON PILOTE",3);
                        Nimprobe::robotconfig::Usertag2("$GBL_NMS_Domain/$HUB/$robot","NON PILOTE");
                    }
                }
            }
            close $file_handler;
        }

        # Failed server BY IP !
        if(not $GBL_Fastretrieving){
            $NMS_Probe->log(YELLOW."Generate local_reject file!",5);
    		$NMS_Probe->localLog("Generate local_reject file!",5);
            my $file_handler;
            unless(open($file_handler,">", "$GBL_STR_CompleteOuputDir/local_reject.txt")) {
                warn "Unabled to open rejected_files \n";
                return;
            }
            foreach my $HUB ( keys %FinalGrappe ) {
                foreach my $robotName ( keys %{ $FinalGrappe{$HUB}{reliqualist} } ) {
                    my $ip      = $FinalGrappe{$HUB}{reliqualist}{$robotName}{ip};
                    my $addr    = $FinalGrappe{$HUB}{reliqualist}{$robotName}{addr};
                    if($ip eq "127.0.0.1" || index($ip, "169.254") != -1) {
                        print $file_handler "$HUB;$robotName;$ip;$addr\n";
                        my @Info = ($HUB,$robotName,$ip,$addr);
                        push($Stats{"server_local"},\@Info);
                    }
                }
            }
            close $file_handler;
        }

        # Server down/maintenance
        if(not $GBL_Fastretrieving){
            $NMS_Probe->log(YELLOW."Generate serverdown file!",5);
    		$NMS_Probe->localLog("Generate serverdown file!",5);
            my $file_handler;
            unless(open($file_handler,">", "$GBL_STR_CompleteOuputDir/serverdown.txt")) {
                warn "Unabled to open rejected_files \n";
                return;
            }
            print $file_handler "HUB;robotName;ip;addr;status\n";
            foreach my $HUB ( keys %FinalGrappe ) {
                foreach my $robotName ( keys %{ $FinalGrappe{$HUB}{reliqualist} } ) {
                    my $status  = $FinalGrappe{$HUB}{reliqualist}{$robotName}{status};
                    my $ip      = $FinalGrappe{$HUB}{reliqualist}{$robotName}{ip};
                    my $addr    = $FinalGrappe{$HUB}{reliqualist}{$robotName}{addr};
                    if($status eq 2 || $status eq 4) {
                        print $file_handler "$HUB;$robotName;$ip;$addr;$status\n";
                        my @Info = ($HUB,$robotName,$ip,$addr,$status);
                        push($Stats{"server_down"},\@Info);
                    }
                }
            }
            close $file_handler;
        }

        # Mise a jour des robots de l'infrastructure!
        if($ARGS_NFO{U}) {
            UpdateRobots(%FinalGrappe);
        }
        pressKey();

        # En fonction des arguments de script on daclenche des sections spacifique au script.
        my %FinalHASH;

        # Move the robot from the loadbalancing schema
        if($ARGS_NFO{M} && not $GBL_Fastretrieving) {
            %FinalHASH = loadBalancer(%FinalGrappe);
            pressKey();

            my %FailedMove = MoveRobots(%FinalHASH);
            if(scalar keys %FailedMove > 0) {
                $NMS_Probe->log(YELLOW."Generate failed_move file!",5);
        		$NMS_Probe->localLog("Generate failed_move file!",5);
                my $file_handler;
                unless(open($file_handler,">", "$GBL_STR_CompleteOuputDir/failed_move.txt")) {
                    warn "Unabled to open failed_move files! \n";
                    return;
                }
                print $file_handler "HUB;robotName;ip;addr;status\n";
                foreach my $HUB ( keys %FailedMove ) {
                    foreach my $RobotName ( keys %{ $FailedMove{$HUB} } ) {
                        my $status  = $FailedMove{$HUB}{$RobotName}{status};
                        my $ip      = $FailedMove{$HUB}{$RobotName}{ip};
                        my $addr    = $FailedMove{$HUB}{$RobotName}{addr};
                        print $file_handler "$HUB;$RobotName;$ip;$addr;$status\n";
                        my @Info = ($HUB,$RobotName,$ip,$addr,$status);
                        push($Stats{"server_failed_move"},\@Info);
                    }
                }
                close $file_handler;
            }
        }
        pressKey();

        # Mise a jour de la sonde net_connect prasente sur les hubs !
        if($ARGS_NFO{N}) {
			if(not $ARGS_NFO{M}) {
				net_config( %FinalGrappe );
			}
			else {
				net_config( %FinalHASH );
			}
        }
        pressKey();

        # On remove des hubs les robots en maintenance / down ou Inactif (CMDB)
        if($ARGS_NFO{R}) {
            if(not $ARGS_NFO{M}) {
				removeReliqua( %FinalGrappe );
			}
			else {
				removeReliqua( %FinalHASH );
			}
        }

        # On forward un Email des machines reliqua !
        if($ARGS_NFO{E}) {
            $Stats{"robot_count"}               = $GBL_STATS_COUNT || 0;
            $Stats{"robot_movement"}            = $GBL_STATS_MOVING || 0;
            $Stats{"robot_movement_success"}    = $GBL_STATS_MOVING_SUCCESS || 0;
            sendEmail(%Stats);
        }
    }
}
main();

# ************************************************* #
# On affiche le temp final du script.
# ************************************************* #
my $GBL_Time_ScriptExecutionTime_End = time();
my $FINAL_TIME = sprintf("$GBL_STR_Time_Format", $GBL_Time_ScriptExecutionTime_End - $GBL_Time_ScriptExecutionTime);
my $Minute      = sprintf("%.2f", $FINAL_TIME / 60);
$NMS_Probe->log(GREEN."\n\nFinal execution time = ".RESET.YELLOW."$Minute minutes !",5);
$NMS_Probe->localLog("Final execution time = $FINAL_TIME second!",5);

# ************************************************* #
# Fin du script
# ************************************************* #
$NMS_Probe->close();

# Copy/Paste the log file !
copy("$GBL_STR_ProbeName.log","$GBL_STR_CompleteOuputDir/$GBL_STR_ProbeName.log") or warn "Failed to copy the log into the final outdir";
