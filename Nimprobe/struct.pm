use strict;
use warnings;

package Nimprobe::struct;
use lib "D:/apps/Nimsoft/perllib";
use lib "D:/apps/Nimsoft/Perl64/lib/Win32API";
use Nimbus::API;
use Nimbus::PDS;
use Nimbus::CFG;
use Term::ANSIColor qw(:constants);
use Win32::Console::ANSI;

our %WarnState = (
    0 => "[CRITICAL]",
	1 => "[ERROR]   ",
	2 => "[WARNING] ",
	3 => "[INFO]    ",
	4 => "[DEBUG]   ",
	5 => "          "
);

my $FH;

sub new {
    my ($class,$name,$version) = @_;
    my $CFG = new Nimbus::CFG("$name.cfg");
    my $this = {
        name        => $name,
        version     => $version,
        loglevel    => $CFG->{'setup'}->{'loglevel'} || 3,
        logfile     => $CFG->{'setup'}->{'logfile'} || "logs/$name.log",
        logsize     => $CFG->{'setup'}->{'logsize'} || 10000,
        logconsole  => $CFG->{'setup'}->{'logconsole'} || 0,
        login       => $CFG->{'setup'}->{'login'} || "administrator",
        password    => $CFG->{'setup'}->{'password'} || "nim76prox",
        sess        => undef
    };
    return bless($this,$class);
}

sub start {
    my $this = shift;
    unless(open($FH,">", "robot_management.log")) {
        warn "Unabled to open super log files! \n";
        return;
    }

    my $rc = nimLogin("$this->{login}","$this->{password}");
    if(not $rc) {
        $this->localLog("Unable to connect to the nimsoft HUB !",0);
        die "Unable to connect to the nimsoft HUB !\n";
    }

    #nimLogSet($this->{logfile},$this->{name},$this->{loglevel},0);
    #nimLogTruncateSize($this->{logsize});

    $this->log("*************************************************",5);
    $this->log("Probe $this->{name} started at ".localtime(),5);
    $this->log("*************************************************",5);
    $this->localLog("*************************************************",5);
    $this->localLog("Probe $this->{name} started at ".localtime(),5);
    $this->localLog("*************************************************",5);

    #$this->{sess} = Nimbus::Session->new("$this->{name}");
    #$this->{sess}->setInfo($this->{version},"Probe custom robot_management");

    #sub timeout {
        #print "probe timeout \n";
    #}

    #sub restart {
        #print "probe restarted! \n";
    #}

    #if ($this->{sess}->server (NIMPORT_ANY,\&timeout,\&restart)==0) {
        #$this->log(GREEN "Registered to the nimsoft hub successfully!",1);
        #$this->localLog("Registered to the nimsoft hub successfully!",1);
    #}
    #else {
        #$this->log(YELLOW."ERR00 - ".RED."Unable to register probe to the Nimsoft HUB!",1);
        #$this->localLog("ERR00 - Unable to register probe to the Nimsoft HUB!",1);
        #exit(1);
    #}
}

sub dateLog {
	my @months = qw( Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec );
	my @days = qw(Sun Mon Tue Wed Thu Fri Sat Sun);
	my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
    my $timetwoDigits = sprintf("%02d %02d:%02d:%02d",$mday,$hour,$min,$sec);
	return "$months[$mon] $timetwoDigits";
}

sub log {
    my ($this,$msg,$level) = @_;
    $level = $level || 3;
    if($level <= $this->{loglevel} && $this->{logconsole}) {
        my $date = $this->dateLog();
        print "$date - $msg\n",RESET;
    }
}

sub localLog {
    my ($this,$msg,$level) = @_;
    $level = $level || 3;
    if($level <= $this->{loglevel}) {
        my $date = $this->dateLog();
        print $FH "$date $WarnState{$level} - $msg\n";
    }
}

sub close {
    my $this = shift;
    $this->localLog("*************************************************",5);
    $this->localLog("Probe $this->{name} ended at ".localtime(),5);
    $this->localLog("*************************************************",5);
    $this->log("*************************************************",5);
    $this->log("Probe $this->{name} ended at ".localtime(),5);
    $this->log("*************************************************",5);
    close $FH;
    nimLogClose();
}

1;
