package Nimprobe::probe;
use strict;
use warnings;
use Exporter qw(import);

use lib "D:/apps/Nimsoft/perllib";
use lib "D:/apps/Nimsoft/Perl64/lib/Win32API";
use Nimbus::API;
use Nimbus::Session;
use Nimbus::CFG;
use Nimbus::PDS;
use Data::Dumper;
use File::Copy;
$Data::Dumper::Indent = 1;

use Term::ANSIColor qw(:constants);
use Win32::Console::ANSI;

our @EXPORT_OK = qw(getCFG updateCFG comparePing createSection PushConfig);

sub createSection {
    my ($CFG,$SECTION) = @_;
    cfgKeyWrite($CFG,$SECTION,"key","value");
    cfgKeyDelete($CFG,$SECTION,"key");
}

sub comparePing {
    my ($old,$new) = @_;
    my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);

    my $old_cfg = cfgOpen($old,0);
    my $new_cfg = cfgOpen($new,0);

    my $HASH_OLD_CFG = Nimbus::CFG->new($old);
    foreach my $profileName (cfgKeyList($new_cfg,"/profiles")) {
        if(exists($HASH_OLD_CFG->{"profiles"}->{$profileName})) {
            my $current_ip = cfgKeyRead($new_cfg,"/profiles/$profileName","ip");
            if($current_ip ne $HASH_OLD_CFG->{"profiles"}->{$profileName}->{"ip"}) {
                print "Rename section : $profileName => ${profileName}_${mon}${mday}${hour} \n";
                cfgSectionRename($new_cfg,"/profiles/$profileName","/profiles/${profileName}_${mon}${mday}${hour}");
            }
        }
    }

    cfgSync($old_cfg);
    cfgClose($old_cfg);

    cfgSync($new_cfg);
    cfgClose($new_cfg);

    return 1;
}

sub updateCFG {
    my ($FILEPATH,$etiquettes,$robotslist) = @_;
    my $CFG = cfgOpen($FILEPATH,0);

    cfgSectionDelete($CFG,"/profiles/");
    cfgSectionDelete($CFG,"/groups/");
    createSection($CFG,"/update_info/");

    createSection($CFG,"/profiles/");
    createSection($CFG,"/groups/");
    createSection($CFG,"/update_info/");
    my $localTime = localtime();
    cfgKeyWrite($CFG,"/update_info/","last_update","$localTime");

    my %refGroup = ();

    # Add groups!
    for my $etiquetteName (keys $etiquettes) {
        cfgKeyWrite($CFG,"/groups/","$etiquetteName","");
        $refGroup{$etiquetteName} = 1;
    }

    # Write profile !
    my $profiles_count = 0;
    for my $robotName (keys $robotslist) {
        $profiles_count++;
        my $robotEtiquette = $robotslist->{$robotName}{etiquette} || "Default";
        if($robotEtiquette eq "") {
            $robotEtiquette = "Default";
        }
        my $robotIp = $robotslist->{$robotName}{ip};
        my $robotPath = "/profiles/$robotName/";
        if( not exists($refGroup{$robotEtiquette}) ) {
            cfgKeyWrite($CFG,"/groups/","$robotEtiquette","");
            $refGroup{$robotEtiquette} = 1;
        }
        createSection($CFG,$robotPath);
        cfgKeyWrite($CFG,$robotPath,"hostname","$robotName");
        cfgKeyWrite($CFG,$robotPath,"active","yes");
        cfgKeyWrite($CFG,$robotPath,"interval","5min");
        cfgKeyWrite($CFG,$robotPath,"QoS","no");
        cfgKeyWrite($CFG,$robotPath,"ip","$robotIp");
        cfgKeyWrite($CFG,$robotPath,"ping","yes");
        cfgKeyWrite($CFG,$robotPath,"alarm","1");
        cfgKeyWrite($CFG,$robotPath,"timeout","1");
        cfgKeyWrite($CFG,$robotPath,"failures","1");
        cfgKeyWrite($CFG,$robotPath,"msg_ok","MsgConnectOk");
        cfgKeyWrite($CFG,$robotPath,"msg_fail","MsgConnectFail");
        cfgKeyWrite($CFG,$robotPath,"icmp_size","0");
        cfgKeyWrite($CFG,$robotPath,"target","2");
        cfgKeyWrite($CFG,$robotPath,"retries","3");
        cfgKeyWrite($CFG,$robotPath,"qos_source","0");
        cfgKeyWrite($CFG,$robotPath,"source","2");
        cfgKeyWrite($CFG,$robotPath,"flags","0");
        cfgKeyWrite($CFG,$robotPath,"icmp_threshold","1000");
        cfgKeyWrite($CFG,$robotPath,"alarm_on_packet_loss","no");
        cfgKeyWrite($CFG,$robotPath,"packets_to_send","4");
        cfgKeyWrite($CFG,$robotPath,"max_packets_lost","2");
        cfgKeyWrite($CFG,$robotPath,"qos_on_packets_lost","no");
        cfgKeyWrite($CFG,$robotPath,"delay_between_packet_to_send","0");
        cfgKeyWrite($CFG,$robotPath,"max_jitter","");
        cfgKeyWrite($CFG,$robotPath,"alarm_on_jitter","0");
        cfgKeyWrite($CFG,$robotPath,"last_update","$localTime");
        cfgKeyWrite($CFG,$robotPath,"group",$robotEtiquette);
    }

    cfgSync($CFG);
    cfgClose($CFG);

    return $profiles_count;
}

sub getCFG {
    my ($robotName,$FILEPATH,$BACKUPFILE) = @_;
    my $PDS_args = pdsCreate();
    pdsPut_PCH ($PDS_args,"directory","probes/network/net_connect/");
    pdsPut_PCH ($PDS_args,"file","net_connect.cfg");
    pdsPut_INT ($PDS_args,"buffer_size",10000000);

    my ($RC, $ProbePDS_CFG) = nimRequest("$robotName", "48000", "text_file_get", $PDS_args,180);
    pdsDelete($PDS_args);

    my $GetRetry = 0;
    while($GetRetry < 3) {
        if($RC == NIME_OK) {
            my $CFG_Handler;

            unless(open($CFG_Handler,">>","$FILEPATH")) {
                warn "\nUnable to create file\n";
                return 0;
            }
            my @ARR_CFG_Config = Nimbus::PDS->new($ProbePDS_CFG)->asHash();
            print $CFG_Handler $ARR_CFG_Config[0]{'file_content'};
            close $CFG_Handler;

            copy("$FILEPATH","$BACKUPFILE") or warn "Copy failed!";

            return 1;
        }
        else {
            print "unabled to get cfg for net_connect on robot $robotName!!\n";
            $GetRetry++;
        }
    }

    return 0;
}

sub PushConfig {
	my ($ADDR,$FILEPATH) = (@_);

    my $FH;
	unless (open ($FH,$FILEPATH)) {
		warn "Unable to open configuration file!\n";
		return 0;
	}

	my $CFG_Content = "";
	while (<$FH>) {
        $CFG_Content .= $_;
    }
    undef $FH;

	my $PDS = pdsCreate();
	pdsPut_PCH($PDS,"directory","probes/network/net_connect/");
	pdsPut_PCH($PDS,"file",'net_connect.cfg');
	pdsPut_PCH($PDS,"file_contents",$CFG_Content);

	my ($RC) = nimNamedRequest("$ADDR/controller", "text_file_put", $PDS);

    return ($RC == NIME_OK) ? 1 : 0;
}
