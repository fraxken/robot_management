package Nimprobe::cmdb;
use strict;
use warnings;
use Exporter qw(import);
use DBI;

use Data::Dumper;
$Data::Dumper::Indent = 1;

our @EXPORT_OK = qw(getLIST);

sub getLIST {
    my ($DB,$cmdb_key) = @_;
    my %ARR_CMDB = ();

    my @CMDB_Opere = split(",",$cmdb_key);
    my $Inner = "";
    my $i=0;
    while ($i<=$#CMDB_Opere) {
        my $Item = $CMDB_Opere[$i];
        if($i == $#CMDB_Opere) {
            $Inner.= "PERIMETRE_OPERE LIKE '%".$Item."%'";
        }
        else {
            $Inner.= "PERIMETRE_OPERE LIKE '%".$Item."%' OR ";
        }
        $i++;
    }
    my $SQL = "SELECT HOSTNAME,SITE,PILOTAGE_OLD,CENTRE_ORGA,SUPERVISION FROM NIMSOFT_AssetDB WHERE ".$Inner;
    my $STH = $DB->prepare($SQL);
    $STH->execute();

    my( $HOSTNAME, $SITE, $PILOTAGE_OLD, $CENTRE_ORGA, $SUPERVISION );
    $STH->bind_columns( undef, \$HOSTNAME, \$SITE, \$PILOTAGE_OLD, \$CENTRE_ORGA, \$SUPERVISION );
    while($STH->fetch()) {
        if(defined($HOSTNAME)) {
            my $NEW_HOST = lc $HOSTNAME;
            my $NEW_SUPERVISION = lc $SUPERVISION;
            $ARR_CMDB{$NEW_HOST} = [
                $SITE,
                $PILOTAGE_OLD,
                $CENTRE_ORGA,
                $NEW_SUPERVISION,
                $NEW_HOST
            ];
        }
    }
    $STH->finish;

    return %ARR_CMDB;
}

sub getEquipements {
    my ($DB,$cmdb_key) = @_;
    my %ARR_CMDB = ();

    my @CMDB_Opere = split(",",$cmdb_key);
    my $Inner = "";
    my $i=0;
    while ($i<=$#CMDB_Opere) {
        my $Item = $CMDB_Opere[$i];
        if($i == $#CMDB_Opere) {
            $Inner.= "PERIMETRE_OPERE LIKE '%".$Item."%'";
        }
        else {
            $Inner.= "PERIMETRE_OPERE LIKE '%".$Item."%' OR ";
        }
        $i++;
    }
    my $STH = $DB->prepare("SELECT HOSTNAME,SITE,SUPERVISION FROM NIMSOFT_AssetDB WHERE ".$Inner." AND SUPERVISION = 'Actif'");
    # $STH->bind_param(1,"%${cmdb_key}%");
    $STH->execute();

    my( $HOSTNAME, $SITE, $PILOTAGE_OLD, $CENTRE_ORGA, $SUPERVISION );
    $STH->bind_columns( undef, \$HOSTNAME, \$SITE, \$SUPERVISION );
    while($STH->fetch()) {
        if(defined($HOSTNAME)) {
            my $NEW_HOST = lc $HOSTNAME;
            my $NEW_SUPERVISION = lc $SUPERVISION;
            $ARR_CMDB{$NEW_HOST} = [
                $SITE,
                $NEW_SUPERVISION,
                $NEW_HOST
            ];
        }
    }
    $STH->finish;

    return %ARR_CMDB;
}
