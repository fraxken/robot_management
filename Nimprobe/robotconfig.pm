package Nimprobe::robotconfig;
use strict;
use warnings;
use Exporter qw(import);

use lib "D:/apps/Nimsoft/perllib";
use lib "D:/apps/Nimsoft/Perl64/lib/Win32API";
use Nimbus::API;
use Nimbus::Session;
use Nimbus::CFG;
use Nimbus::PDS;

use Term::ANSIColor qw(:constants);
use Win32::Console::ANSI;

our @EXPORT_OK = qw(Origin Usertag2 RestartController RemoveRobot MoveRobot RestartProbe Probe_deactivate Probe_activate);

sub Origin {
	my ($ROBOT_PATH, $ORIGIN) = @_;
	my $PDS = pdsCreate();

    # Prepare PDS
	pdsPut_PCH ($PDS,"name","spooler");
	pdsPut_PCH ($PDS,"section","spooler");
	pdsPut_PCH ($PDS,"key","origin");
	pdsPut_PCH ($PDS,"value",$ORIGIN);

	my ($RC, $OBJ) = nimNamedRequest("$ROBOT_PATH/controller", "probe_config_set", $PDS,2);
	pdsDelete($PDS);

	return ($RC == NIME_OK) ? 1 : 0;
}

sub Domain {
	my ($ROBOT_PATH, $DOMAIN) = @_;
    # Prepare PDS
	my $PDS = pdsCreate();
	my $PDS_OPTION = pdsCreate();

    # Prepare PDS
	pdsPut_PCH ($PDS_OPTION,"/controller/domain",$DOMAIN) ;
	pdsPut_PCH ($PDS,"name","controller");
	pdsPut_PDS ($PDS,"as_pds",$PDS_OPTION);

	my ($RC, $OBJ) = nimNamedRequest("$ROBOT_PATH/controller", "probe_config_set", $PDS,2);
	pdsDelete($PDS);

	return ($RC == NIME_OK) ? 1 : 0;
}

sub Usertag2 {
	my ($ROBOT_PATH, $PILOTAGE_OLD) = @_;
	my $PDS = pdsCreate();
	my $PDS_OPTION = pdsCreate();

    # Prepare PDS
	pdsPut_PCH ($PDS_OPTION,"/controller/os_user2",$PILOTAGE_OLD) ;
	pdsPut_PCH ($PDS,"name","controller");
	pdsPut_PDS ($PDS,"as_pds",$PDS_OPTION);

	my ($RC, $OBJ) = nimNamedRequest("$ROBOT_PATH/controller", "probe_config_set", $PDS,2);
	pdsDelete($PDS_OPTION);
	pdsDelete($PDS);

	return ($RC == NIME_OK) ? 1 : 0;
}

sub RestartController {
	my $ROBOT_FULLPATH = shift;
	my $PDS = pdsCreate();
	my ($RC,$OBJ) = nimNamedRequest( "$ROBOT_FULLPATH/controller", "_restart",$PDS,2);
	pdsDelete($PDS);
	return ($RC == NIME_OK) ? 1 : 0;
}

sub RemoveRobot {
	my ($ROBOT_FULLPATH, $NAME) = @_;
	my $PDS = pdsCreate();

	pdsPut_PCH ($PDS,"name",$NAME);
	pdsPut_PCH ($PDS,"name","controller");

	my ($RC, $OBJ) = nimNamedRequest("$ROBOT_FULLPATH/hub", "removerobot", $PDS,2);
	pdsDelete($PDS);

    return ($RC == NIME_OK) ? 1 : 0;
}

sub Probe_deactivate {
	my ($ROBOT_FULLPATH, $PROBE) = (@_);
	my $PDS = pdsCreate();
	pdsPut_PCH($PDS,"name",$PROBE);
	pdsPut_INT($PDS,"noforce",1);
	my ($RC,$OBJ) = nimNamedRequest( "$ROBOT_FULLPATH/controller", "probe_deactivate",$PDS,2);
	return ($RC == NIME_OK) ? 1 : 0;
}

sub Probe_activate {
	my ($ROBOT_FULLPATH, $PROBE) = (@_);
	my $PDS = pdsCreate();
	pdsPut_PCH($PDS,"name",$PROBE);
	my ($RC,$OBJ) = nimNamedRequest( "$ROBOT_FULLPATH/controller", "probe_activate",$PDS,2);
	return ($RC == NIME_OK) ? 1 : 0;
}

sub RestartProbe {
	my ($ROBOT_FULLPATH, $PROBE) = (@_);
	my $PDS = pdsCreate();
	my ($RC,$OBJ) = nimNamedRequest( "$ROBOT_FULLPATH/$PROBE", "_restart",$PDS,2);
	pdsDelete($PDS);
	print "probe RS $RC \n";
	return ($RC == NIME_OK) ? 1 : 0;
}

sub updateSecondary {
	my ($Source_HubName,$RobotName,$NMS_Domain,$Secondary_HubName,$Secondary_HubServerName,$ADDR) = @_;

	my $PDS_CONFIG = pdsCreate();
	pdsPut_PCH($PDS_CONFIG, "/controller/secondary_domain", "$NMS_Domain");
	pdsPut_PCH($PDS_CONFIG, "/controller/secondary_hub", "$Secondary_HubName");
	pdsPut_PCH($PDS_CONFIG, "/controller/secondary_hubrobotname", "$Secondary_HubServerName");
	pdsPut_PCH($PDS_CONFIG, "/controller/temporary_hub_broadcast","no");
	pdsPut_PCH($PDS_CONFIG, "/controller/secondary_hubip", "$Secondary_HubServerName");
	pdsPut_PCH($PDS_CONFIG, "/controller/secondary_hubport", "48002");

	my $PDS = pdsCreate();
	pdsPut_PCH ($PDS,"name","controller");
	pdsPut_PDS ($PDS,"as_pds",$PDS_CONFIG);

	my ($RC, $O) = nimNamedRequest("$ADDR/controller", "probe_config_set", $PDS);
	pdsDelete($PDS);
	pdsDelete($PDS_CONFIG);

	if ($RC == NIME_OK)  {
		return 1;
	}
	else {
		return 0;
	}

}

sub Maintenance {
	my ($ADDR,$SupervisionState) = @_;
	if ($SupervisionState eq "inactif") {
		my $PDS = pdsCreate();
		pdsPut_INT ($PDS,"for",31536000) ;
		pdsPut_INT ($PDS,"until",0) ;
		pdsPut_PCH ($PDS,"comment","AssetState = Not In Use");
		my ($RC, $OBJ) = nimNamedRequest("$ADDR/controller", "maint_until", $PDS);
		pdsDelete($PDS);
		return ($RC == NIME_OK) ? 1 : 0;
	}
	elsif ($SupervisionState eq "actif") {
		my $PDS = pdsCreate();
		pdsPut_INT ($PDS,"for",0) ;
		pdsPut_INT ($PDS,"until",0) ;
		pdsPut_PCH ($PDS,"comment","AssetState = In Use");
		my ($RC, $OBJ) = nimNamedRequest("$ADDR/controller", "maint_until", $PDS);
		pdsDelete($PDS);
		return ($RC == NIME_OK) ? 1 : 0;
	}
}

sub MoveRobot {
	my ($NMS_Domain, $Source_HubName, $Primary_HubName, $Primary_HubServerName, $Secondary_HubName, $Secondary_HubServerName,$RobotName,$NMS_Probe) = (@_);

	my $PDS_OPTIONS = pdsCreate();
	pdsPut_PCH($PDS_OPTIONS, "/controller/secondary_domain", "");
	pdsPut_PCH($PDS_OPTIONS, "/controller/secondary_hub", "");
	pdsPut_PCH($PDS_OPTIONS, "/controller/secondary_hubrobotname", "");
	pdsPut_PCH($PDS_OPTIONS, "/controller/temporary_hub_broadcast","no");
	pdsPut_PCH($PDS_OPTIONS, "/controller/secondary_hubip", "");
	pdsPut_PCH($PDS_OPTIONS, "/controller/secondary_hubport", "48002");
	pdsPut_PCH($PDS_OPTIONS, "/controller/secondary_hub_dns_name","");

	my $PDS_ARGS = pdsCreate();
	pdsPut_PCH ($PDS_ARGS,"name","controller");
	pdsPut_PDS ($PDS_ARGS,"as_pds",$PDS_OPTIONS);

	my ($RC, $OBJ) = nimNamedRequest("/$NMS_Domain/$Source_HubName/$RobotName/controller", "probe_config_set", $PDS_ARGS);
	pdsDelete($PDS_OPTIONS);
	pdsDelete($PDS_ARGS);

    undef $PDS_ARGS;
    undef $PDS_OPTIONS;

	if ($RC != NIME_OK) {
		$NMS_Probe->localLog("Failed to call the callback probe_config_set!");
		print "Failed to call the callback probe_config_set!\n";
		return 0;
	}
    undef $RC;

    # Restart controller !
	RestartController("/$NMS_Domain/".$Source_HubName."/".$RobotName);

	my $PDS_OPTION2 = pdsCreate();
	pdsPut_PCH($PDS_OPTION2, "/controller/secondary_domain", $NMS_Domain);
	pdsPut_PCH($PDS_OPTION2, "/controller/secondary_hub", $Secondary_HubName);
	pdsPut_PCH($PDS_OPTION2, "/controller/secondary_hubrobotname", $Secondary_HubServerName);
	pdsPut_PCH($PDS_OPTION2, "/controller/temporary_hub_broadcast","no");
	pdsPut_PCH($PDS_OPTION2, "/controller/secondary_hubip", $Secondary_HubServerName);
	pdsPut_PCH($PDS_OPTION2, "/controller/secondary_hubport", "48002");
	pdsPut_PCH($PDS_OPTION2, "/controller/secondary_hub_dns_name","");

	my $PDS_ARGS2 = pdsCreate();
	pdsPut_PCH ($PDS_ARGS2,"name","controller");
	pdsPut_PDS ($PDS_ARGS2,"as_pds",$PDS_OPTION2);

	my ($RC2, $OB2J) = nimNamedRequest("/$NMS_Domain/$Source_HubName/$RobotName/controller", "probe_config_set", $PDS_ARGS2);
	pdsDelete($PDS_OPTION2);
	pdsDelete($PDS_ARGS2);

    if ($RC2 != NIME_OK)  {
		$NMS_Probe->localLog("Failed to call the callback probe_config_set! $RC2 [X2]");
		print "Failed to call the callback probe_config_set $RC2 [X2]\n";
		return 0;
	}

	my $PDS_ARGS3 = pdsCreate();
	pdsPut_PCH ($PDS_ARGS3,"hubdomain",$NMS_Domain);
	pdsPut_PCH ($PDS_ARGS3,"hubname",$Primary_HubName);
	pdsPut_PCH ($PDS_ARGS3, "hubip", $Primary_HubServerName);
	pdsPut_PCH ($PDS_ARGS3, "hubport", "");
	pdsPut_PCH ($PDS_ARGS3, "hub_dns_name",$Primary_HubServerName );
	pdsPut_PCH ($PDS_ARGS3, "robotip_alias", "");

	my ($RC3, $OBJ3) = nimNamedRequest("/$NMS_Domain/$Source_HubName/$RobotName/controller", "sethub", $PDS_ARGS3);

	if($RC3 != NIME_OK) {
		print RED."Failed to put configuration of primary hub! \n".RESET;
		$NMS_Probe->localLog("Failed to put configuration of primary hub!");
	}

	return ($RC3 == NIME_OK) ? 1 : 0;
}
